//
//  FAQAnswerViewController.h
//  LittleThings
//
//  Created by Saurav on 04/04/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"

@interface FAQAnswerViewController : UIViewController<APIsDelegate>
{
    WebAPI *api;
}

@property (strong, nonatomic) NSDictionary *selectedQuestion;

@property (strong, nonatomic) IBOutlet UILabel *question;
@property (strong, nonatomic) IBOutlet UITextView *answer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *questionHeight;
- (IBAction)back:(id)sender;
@end
