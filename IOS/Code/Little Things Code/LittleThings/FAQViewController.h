//
//  FAQViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "SWRevealViewController.h"
#import "FAQAnswerViewController.h"

@interface FAQViewController : UIViewController<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, APIsDelegate>
{
    NSArray *faqList, *searchList;
    BOOL isSearching;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UISearchBar *serach;
@property (strong, nonatomic) IBOutlet UITableView *table;
- (IBAction)back:(id)sender;
@end
