//
//  SinchVideoHelper.m
//  SinchVideoCallDemo
//
//  Created by Saurav on 06/01/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import "SinchVideoHelper.h"

@implementation SinchVideoHelper

+ (instancetype)shareInstance {
    static SinchVideoHelper *shareInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[SinchVideoHelper alloc] init];
    });
    return shareInstance;
}

- (void)initSinchClientWithUserId:(NSString *)userId
{
    if (!_client) {
        _client = [Sinch clientWithApplicationKey:@"67e38f88-f2af-427b-a485-7389feb3141c"
                                                  applicationSecret:@"K9/kB9JiaEe21Z9khQisnw=="
                                                    environmentHost:@"sandbox.sinch.com"
                                                             userId:userId];
        _client.delegate = self;
        [_client setSupportMessaging:YES];
        [_client setSupportPushNotifications:YES];
        [_client enableManagedPushNotifications];
        [_client setSupportActiveConnectionInBackground:YES];
        [_client start];    
    }
}

- (id<SINCall>)callSinchClientWithUserDetails:(NSDictionary *)userDetails
{
    NSString *imageURL=@"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"image"])
    {
        imageURL=[[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    }
    NSDictionary *headers=[[NSDictionary alloc]initWithObjects:@[[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]], imageURL, [[NSUserDefaults standardUserDefaults] objectForKey:@"name"]] forKeys:@[@"FriendID", @"Image", @"Name"]];
    id<SINCallClient> callClient = [_client callClient];
    id<SINCall> call = [callClient callUserVideoWithId:[NSString stringWithFormat:@"NextGen-%@", [userDetails objectForKey:@"FriendID"]] headers:headers];
    
    return call;
}

- (NSString *)pathForSound:(NSString *)soundName
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:soundName];
}

#pragma mark -- SinClientDelegate methods

-(void)clientDidStart:(id<SINClient>)client
{
    NSLog(@"client started");
}

-(void)clientDidFail:(id<SINClient>)client error:(NSError *)error
{
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
}

@end
