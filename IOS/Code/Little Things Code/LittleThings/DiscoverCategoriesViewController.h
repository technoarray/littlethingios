//
//  DiscoverCategoriesViewController.h
//  LittleThings
//
//  Created by Saurav on 03/04/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import <UIImageView+WebCache.h>
#import "DiscoverViewController.h"
#import "DashboardViewController.h"

@interface DiscoverCategoriesViewController : UIViewController<APIsDelegate, UITableViewDelegate, UITableViewDataSource>
{
    NSArray *list;
    WebAPI *api;
}

@property (strong, nonatomic) NSDictionary *selectedCategory;

@property (strong, nonatomic) IBOutlet UIButton *sideMenu;
@property (strong, nonatomic) IBOutlet UITableView *table;
@end
