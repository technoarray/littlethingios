//
//  DiscoverCategoriesViewController.m
//  LittleThings
//
//  Created by Saurav on 03/04/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "DiscoverCategoriesViewController.h"

@interface DiscoverCategoriesViewController ()

@end

@implementation DiscoverCategoriesViewController

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    appDelegate.showChatScreen=NO;
    NSLog(@"appear");
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        if (list.count==0)
        {
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        }
        if (_selectedCategory)
        {
            [api getDiscoverSubcategoriesForCategoryID:[_selectedCategory objectForKey:@"cat_id"]];
        }
        else
        {
            [api getDiscoverCategories];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_selectedCategory)
    {
        [_sideMenu setImage:[UIImage imageNamed:@"right-arrow.png"] forState:UIControlStateNormal];
        [_sideMenu addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [_sideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OpenCalendar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCalendar) name:@"OpenCalendar" object:nil]; //
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openChat) name:@"Chat" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowNotificationBanner" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNotificationBanner) name:@"ShowNotificationBanner" object:nil];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void) callBackDiscoverCategoriesSuccess:(id)response
{
    list = [response objectForKey:@"discover_info"];
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    NSDictionary *dict = [list objectAtIndex:indexPath.row];
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"newCategoryCell"];
    
    UILabel *title = [cell viewWithTag:1];
    UIImageView *categoryImage = [cell viewWithTag:2];
    UIActivityIndicatorView *indicator = [cell viewWithTag:3];
    UIView *blackView = [cell viewWithTag:4];
    
    [indicator startAnimating];
    [indicator setHidden:NO];
    [indicator setHidesWhenStopped:YES];
    
    if (_selectedCategory)
    {
        title.text = [dict objectForKey:@"sub_cat_name"];
        [categoryImage sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"sub_cat_image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [indicator stopAnimating];
             categoryImage.image = image;
         }];
    }
    else
    {
        title.text = [dict objectForKey:@"cat_name"];
        [categoryImage sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"cat_image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            categoryImage.image = image;
            [indicator stopAnimating];
        }];
    }
    
    
    
    CGRect rect = [Helper calculateHeightForText:title.text fontName:title.font.fontName fontSize:title.font.pointSize maximumWidth:self.view.frame.size.width-120];
    for (NSLayoutConstraint *constraint in blackView.constraints)
    {
        if ([constraint.identifier isEqualToString:@"blackViewWidthConstraint"])
        {
            constraint.constant = rect.size.width+25;
            break;
        }
    }
    
    for (NSLayoutConstraint *constraint in title.constraints)
    {
        if ([constraint.identifier isEqualToString:@"nameWidthConstraint"])
        {
            constraint.constant = rect.size.width+5;
            break;
        }
    }
    
    if (rect.size.height>25)
    {
        for (NSLayoutConstraint *constraint in title.constraints)
        {
            if ([constraint.identifier isEqualToString:@"nameHeightConstraint"])
            {
                constraint.constant = rect.size.height+5;
                break;
            }
        }
        
        for (NSLayoutConstraint *constraint in blackView.constraints)
        {
            if ([constraint.identifier isEqualToString:@"blackViewHeightConstraint"])
            {
                constraint.constant = rect.size.height+5+22;
                break;
            }
        }
    }
    else
    {
        for (NSLayoutConstraint *constraint in title.constraints)
        {
            if ([constraint.identifier isEqualToString:@"nameHeightConstraint"])
            {
                constraint.constant = 20;
                break;
            }
        }
        
        for (NSLayoutConstraint *constraint in blackView.constraints)
        {
            if ([constraint.identifier isEqualToString:@"blackViewHeightConstraint"])
            {
                constraint.constant = 42;
                break;
            }
        }
    }//descriptionHeightConstraint
    
    title.numberOfLines=3;
    
    [cell.contentView bringSubviewToFront:title];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedCategory)
    {
        DiscoverViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverViewController"];
        controller.selectedSubcategory = [list objectAtIndex:indexPath.row];
        controller.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        DiscoverCategoriesViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverCategoriesViewController"];
        controller.selectedCategory = [list objectAtIndex:indexPath.row];
        controller.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170;
}

#pragma mark -- Action Methods

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Notification Method

-(void)openCalendar
{
    self.tabBarController.selectedIndex=2;
}

-(void)openChat
{
    self.tabBarController.selectedIndex=1;
    [self performSelector:@selector(showChat) withObject:nil afterDelay:0.5f];
    
}

-(void)showChat{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    UINavigationController *nav=[self.tabBarController.viewControllers objectAtIndex:1];
    DashboardViewController *view = [nav.childViewControllers objectAtIndex:0];
    if (view.messages != nil) {
        if (appDelegate.messageReceived != nil) {
            [view.messages addObject:@[appDelegate.messageReceived, @(0) ]];
            [view.table reloadData];
            [view scrollToBottom];
        }
    }
}

-(void)showNotificationBanner
{
    //self.tabBarController.selectedIndex=1;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
