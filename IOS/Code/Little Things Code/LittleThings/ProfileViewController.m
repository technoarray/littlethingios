//
//  ProfileViewController.m
//  LittleThings
//
//  Created by Saurav on 17/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

-(void)viewWillAppear:(BOOL)animated
{
    _userName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"])
    {
        NSData* data = [[NSData alloc] initWithBase64EncodedString:[[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage* image = [UIImage imageWithData:data];
        _userImage.image = image;
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"image"])
    {
        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             _userImage.image = image;
         }];
    }
    [_table reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    _userImage.layer.cornerRadius = 86/2;
    _userImage.clipsToBounds = YES;
    _userImage.layer.borderColor=[UIColor whiteColor].CGColor;
    _userImage.layer.borderWidth=3;
    
    _messageBtn.layer.cornerRadius = 35/2;
    _messageBtn.clipsToBounds = YES;
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
    
    UILabel *title = [cell viewWithTag:1];
    UILabel *info = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0:
            title.text=@"Email";
            info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
            break;
        case 1:
            title.text=@"Phone Number";
            info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"phone_number"];
            break;
        case 2:
            title.text=@"Gender";
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] length]>0)
            {
                info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"];
            }
            else
            {
                info.text=@"Not Added";
            }
            break;
        case 3:
            title.text=@"Address";
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"address"] length]>0)
            {
                info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
            }
            else
            {
                info.text=@"Not Added";
            }
            break;
        case 4:
            title.text=@"City";
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"city"] length]>0)
            {
                info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"city"];
            }
            else
            {
                info.text=@"Not Added";
            }
            break;
        case 5:
            title.text=@"State";
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"state"] length]>0)
            {
                info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"state"];
            }
            else
            {
                info.text=@"Not Added";
            }
            break;
        case 6:
            title.text=@"Zip";
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"zipcode"] length]>0)
            {
                info.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"zipcode"];
            }
            else
            {
                info.text=@"Not Added";
            }
            break;
            
        default:
            break;
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

#pragma mark -- Action Methods

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)message:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Chat" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editProfile:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateProfileViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
