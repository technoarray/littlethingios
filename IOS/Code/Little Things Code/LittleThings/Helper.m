//
//  Helper.m
//  browze
//
//  Created by HashBrown Systems on 26/03/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import "Helper.h"
#import "SinchVideoHelper.h"

@implementation Helper

+(void)saveAllValuesToUserDefault:(NSDictionary *)dic
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%@", [dic objectForKey:@"uid"]] forKey:@"MyId"];
    [defaults setObject:[dic objectForKey:@"name"] forKey:@"name"];
    [defaults setObject:[dic objectForKey:@"email"] forKey:@"email"];
    [defaults setObject:[dic objectForKey:@"phone_number"] forKey:@"phone_number"];
    [defaults setObject:[dic objectForKey:@"address"] forKey:@"address"];
    [defaults setObject:[dic objectForKey:@"city"] forKey:@"city"];
    [defaults setObject:[dic objectForKey:@"customer_id"] forKey:@"customer_id"];
    [defaults setObject:[dic objectForKey:@"credit"] forKey:@"credit"];
    [defaults setObject:[dic objectForKey:@"state"] forKey:@"state"];
    [defaults setObject:[dic objectForKey:@"zipcode"] forKey:@"zipcode"];
    if (![[dic objectForKey:@"profile_image"] isKindOfClass:[NSNull class]] || [[dic objectForKey:@"profile_image"] length]>0)
    {
        [defaults setObject:[dic objectForKey:@"profile_image"] forKey:@"image"];
    }
    
    if (![[dic objectForKey:@"gender"] isKindOfClass:[NSNull class]])
    {
        [defaults setObject:[dic objectForKey:@"gender"] forKey:@"gender"];
    }
    else
    {
        [defaults setObject:@"" forKey:@"gender"];
    }
    
    [defaults synchronize];
}

+(NSNumber *)getNumberFromString:(NSString *)strValue
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    NSNumber *number = [f numberFromString:[NSString stringWithFormat:@"%@", strValue]];
    return number;
}

+(NSString *)getDateStringFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSLog(@"GMT date: %@", dateString);
    return dateString;
}

+ (NSDate *)getDateFromDateString:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm a"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
}

+(NSString *)calculatePlainTextTimeFromSeconds:(float)seconds
{
    NSString *time;
    if (seconds>3599)
    {
        long hours=seconds/3600;
        long remaingMinute=seconds-(hours*3600);
        long minutes=remaingMinute/60;
        long lastSeconds=remaingMinute-(minutes*60);
        
        NSString *hourText=(hours>9?[NSString stringWithFormat:@"%lu", hours]:[NSString stringWithFormat:@"0%lu", hours]);
        
        NSString *minuteText=(minutes>9?[NSString stringWithFormat:@"%lu", minutes]:[NSString stringWithFormat:@"0%lu", minutes]);
        
        NSString *secondsText=(lastSeconds>9?[NSString stringWithFormat:@"%lu", lastSeconds]:[NSString stringWithFormat:@"0%lu", lastSeconds]);
        
        time=[NSString stringWithFormat:@"%@:%@:%@", hourText, minuteText, secondsText];
    }
    else if (seconds>60)
    {
        long minutes=seconds/60;
        long lastSeconds=seconds-(minutes*60);
        
        NSString *minuteText=(minutes>9?[NSString stringWithFormat:@"%lu", minutes]:[NSString stringWithFormat:@"0%lu", minutes]);
        
        NSString *secondsText=(lastSeconds>9?[NSString stringWithFormat:@"%lu", lastSeconds]:[NSString stringWithFormat:@"0%lu", lastSeconds]);
        
        time=[NSString stringWithFormat:@"%@:%@", minuteText, secondsText];
    }
    else
    {
        long sec=seconds;
        NSString *secondsText=(seconds>9?[NSString stringWithFormat:@"%ld", sec]:[NSString stringWithFormat:@"0%ld", sec]);
        
        time=[NSString stringWithFormat:@"00:%@", secondsText];
    }
    return time;
}

+(NSString *)getJSONStringFromDictionary:(NSDictionary *)params
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

+(NSData *)getDataFromDictionary:(NSDictionary *)params
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    return jsonData;
}

+(NSString *)getJSONDtringFromData:(NSData *)data
{
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonString;
}

+(NSString *)convertDateForToLbl:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM dd, yyyy"];
    NSString *newDateString = [dateFormat stringFromDate:date];
    
    NSArray *commaList = [newDateString componentsSeparatedByString:@","];
    NSArray *firstPartList = [[commaList firstObject] componentsSeparatedByString:@" "];
    
    int day = [[firstPartList lastObject] intValue];
    
    NSString *dayStr;
    if (day<10)
    {
        dayStr = [NSString stringWithFormat:@"0%d", day];
    }
    else
    {
        dayStr = [NSString stringWithFormat:@"%d", day];
    }
    
    if (day == 1 || day == 21 || day == 31)
    {
        dayStr = [NSString stringWithFormat:@"%@st", dayStr];
    }
    else if (day == 2 || day == 22)
    {
        dayStr = [NSString stringWithFormat:@"%@nd", dayStr];
    }
    else if (day == 3 || day == 23)
    {
        dayStr = [NSString stringWithFormat:@"%@rd", dayStr];
    }
    else
    {
        dayStr = [NSString stringWithFormat:@"%@th", dayStr];
    }
    
    NSString *stringToReturn = [NSString stringWithFormat:@"%@ %@, %@", [firstPartList firstObject], dayStr, [commaList lastObject]];
    
    return stringToReturn;
}

+(NSString *)convertDateForAPI:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];
    NSString *newDateString = [dateFormat stringFromDate:date];
    return newDateString;
}

+(float)fontSizeForCurrentViewHeight:(float)viewHeight baseHeight:(float)baseHeight baseFontSize:(float)baseFontSize
{
    float heightRatio=viewHeight/baseHeight;
    float fontSize=heightRatio*baseFontSize;
    return fontSize;
}

+(float)calculateViewWidthForView:(float)viewHeight baseWidth:(float)baseWidth baseFontSize:(float)baseFontSize
{
    float heightRatio=viewHeight/baseWidth;
    float fontSize=heightRatio*baseFontSize;
    return fontSize;
}

+(NSString *)getNotificationToken
{
    NSString *hexToken;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    hexToken=[defaults objectForKey:@"token"];
    if (!hexToken)
    {
        hexToken=@"1234567890";
    }
    NSLog(@"Token Data %@",hexToken);
    return hexToken;
}

+(NSString *)getIMEINumber
{
    UIDevice *myDevice = [UIDevice currentDevice];
    NSUUID *identifier = myDevice.identifierForVendor;
    NSString *imei=identifier.UUIDString;
    return imei;
}

+(CGRect)calculateHeightForText:(NSString *)text fontName:(NSString *)fontName fontSize:(float)fontSize maximumWidth:(float)width
{
    UIFont *font = [UIFont fontWithName:fontName size:fontSize];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    
    return rect;
}

+(CGRect)calculateHeightForAttributedText:(NSAttributedString *)text font:(UIFont *)font maximumWidth:(float)width
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect;
}

+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

+(BOOL)validateEmailWithString:(NSString*)emailmatch
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailmatch rangeOfString:@" "].location==NSNotFound && [emailTest evaluateWithObject:emailmatch])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(BOOL)validateUsernameWithString:(NSString*)username
{
    NSCharacterSet * characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString: username];
    if([[NSCharacterSet alphanumericCharacterSet] isSupersetOfSet: characterSetFromTextField] == NO)
    {
        NSLog( @"there are bogus characters here, throw up a UIAlert at this point");
        return NO;
    }
    else
    {
        return YES;
    }
}

+(void)changeColorOfImageToAppColor:(UIImageView *)imageView
{
    [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [imageView setTintColor:APP_COLOR];
}

+(BOOL)removeAllKeysFromUserDefaults
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"MyId"];
    [defaults removeObjectForKey:@"name"];
    [defaults removeObjectForKey:@"phone_number"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"gender"];
    [defaults removeObjectForKey:@"address"];
    [defaults removeObjectForKey:@"city"];
    [defaults removeObjectForKey:@"customer_id"];
    [defaults removeObjectForKey:@"credit"];
    [defaults removeObjectForKey:@"imageString"];
    [defaults removeObjectForKey:@"image"];
    [defaults removeObjectForKey:@"state"];
    [defaults removeObjectForKey:@"zipcode"];
    
    [defaults synchronize];
    
    return YES;
}

+(void)registerForSinch
{
    [[SinchVideoHelper shareInstance] initSinchClientWithUserId:[NSString stringWithFormat:@"LittleThings-%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]]];
}

#pragma mark -- Internet Connectivity

+(BOOL) isInternetConnected
{
    // called after network status changes
    Reachability* internetReachable = [Reachability reachabilityForInternetConnection];;
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            // NSLog(@"The internet is down.");
            return NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            // NSLog(@"The internet is working via WIFI.");
            return YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            // NSLog(@"The internet is working via WWAN.");
            return YES;
            
            break;
        }
    }
}

#pragma mark -- View Indicator

+(void)showIndicatorWithText:(NSString *)text inView:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = text;
}

+(void)hideIndicatorFromView:(UIView *)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

#pragma mark -- get size
+(CGSize )GetcgSizeFromNsstring:(NSString *)strText withFont:(UIFont *)font withCGSizeMake:(CGSize )CGSizeMakeForFrame{
    NSString *strValue=@"";
    if([strText isKindOfClass:[NSNull class]] || [strText isEqualToString:@"<null>"]){
        strValue=@"";
    }else{
        strValue=[NSString stringWithFormat:@"%@",strText];
    }
    CGSize sizeList;
    NSString *version = [[UIDevice currentDevice] systemVersion];
    float floVersion=[version floatValue];
    if(floVersion>=7.0){
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:font forKey: NSFontAttributeName];
        sizeList = [strValue boundingRectWithSize:CGSizeMakeForFrame options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:stringAttributes context:nil].size;
        stringAttributes=nil;
    }else{
        sizeList=[strValue sizeWithFont:font constrainedToSize:CGSizeMakeForFrame lineBreakMode:NSLineBreakByWordWrapping];
    }
    return sizeList;
}

@end
