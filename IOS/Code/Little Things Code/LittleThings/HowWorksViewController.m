//
//  HowWorksViewController.m
//  LittleThings
//
//  Created by Saurav on 31/05/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "HowWorksViewController.h"

@interface HowWorksViewController ()

@end

@implementation HowWorksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    CGRect rect = [Helper calculateHeightForText:_question.text fontName:_question.font.fontName fontSize:_question.font.pointSize maximumWidth:self.view.frame.size.width-40];
    if (rect.size.height>20)
    {
        _questionHeightConstraint.constant = rect.size.height+8;
    }
    
    rect = [Helper calculateHeightForText:_example.text fontName:_example.font.fontName fontSize:_example.font.pointSize maximumWidth:self.view.frame.size.width-40];
    if (rect.size.height>20)
    {
        _exampleHeightConstraint.constant = rect.size.height+8;
    }
    
    rect = [Helper calculateHeightForText:_firstPoint.text fontName:_firstPoint.font.fontName fontSize:_firstPoint.font.pointSize maximumWidth:self.view.frame.size.width-92];
    if (rect.size.height>20)
    {
        _firstHeightConstraint.constant = rect.size.height+8;
    }
    
    rect = [Helper calculateHeightForText:_secondWidth.text fontName:_secondWidth.font.fontName fontSize:_secondWidth.font.pointSize maximumWidth:self.view.frame.size.width-92];
    if (rect.size.height>20)
    {
        _secondHeightConstraint.constant = rect.size.height+8;
    }
    
    rect = [Helper calculateHeightForText:_thirdPoint.text fontName:_thirdPoint.font.fontName fontSize:_thirdPoint.font.pointSize maximumWidth:self.view.frame.size.width-92];
    if (rect.size.height>20)
    {
        _thirdHeightConstraint.constant = rect.size.height+8;
    }
    
    _contentViewHeightConstraint.constant = 650;
}

#pragma mark -- memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buyCredits:(id)sender
{
    PaymentDetailsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentDetailsViewController"];
    controller.isBuyingCredits=_isBuyingCredits;
    controller.showBackBtn=_showBackBtn;
    controller.selectedCredit=_creditInfo;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)applyCode:(id)sender
{
    if (_couponTxt.text.length>0)
    {
        if ([Helper isInternetConnected]==YES)
        {
            [Helper showIndicatorWithText:@"Applying..." inView:self.view];
            [api applyCouponCode:_couponTxt.text];
        }
        else
        {
            [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
        }
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackApplyCouponSuccess:(id)response
{
    PaymentDetailsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentDetailsViewController"];
    controller.isBuyingCredits=_isBuyingCredits;
    controller.showBackBtn=_showBackBtn;
    controller.selectedCredit=_creditInfo;
    controller.discountDetails=response;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
