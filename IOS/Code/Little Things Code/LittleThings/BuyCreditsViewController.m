//
//  BuyCreditsViewController.m
//  LittleThings
//
//  Created by Saurav on 31/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "BuyCreditsViewController.h"

@interface BuyCreditsViewController ()

@end

@implementation BuyCreditsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (_showBackBtn==YES)
    {
        [_menuBtn setImage:[UIImage imageNamed:@"right-arrow.png"] forState:UIControlStateNormal];
        [_menuBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    creditList = [[NSMutableArray alloc]init];
    
    for (int i=0; i<4; i++)
    {
        if (i==0)
        {
            NSDictionary *dict = [[NSDictionary alloc]initWithObjects:@[@"25", @"175", @"4 Hours", @"25"] forKeys:@[@"credits", @"price", @"hours", @"number_of_credits"]];
            [creditList addObject:dict];
        }
        else if (i==1)
        {
            NSDictionary *dict = [[NSDictionary alloc]initWithObjects:@[@"50", @"325", @"8 Hours", @"50"] forKeys:@[@"credits", @"price", @"hours", @"number_of_credits"]];
            [creditList addObject:dict];
        }
        else if (i==2)
        {
            NSDictionary *dict = [[NSDictionary alloc]initWithObjects:@[@"75", @"475", @"12 Hours", @"75"] forKeys:@[@"credits", @"price", @"hours", @"number_of_credits"]];
            [creditList addObject:dict];
        }
        else
        {
            NSDictionary *dict = [[NSDictionary alloc]initWithObjects:@[@"100", @"625", @"16 Hours", @"100"] forKeys:@[@"credits", @"price", @"hours", @"number_of_credits"]];
            [creditList addObject:dict];
        }
    }
    
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return creditList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newCreditCell"];
    
    UILabel *credits = [cell viewWithTag:1];
    UILabel *price = [cell viewWithTag:2];
    UILabel *hours = [cell viewWithTag:3];
    
    NSDictionary *dict = [creditList objectAtIndex:indexPath.row];
    credits.text=[dict objectForKey:@"credits"];
    price.text=[NSString stringWithFormat:@"$%@", [dict objectForKey:@"price"]];
    hours.text=[dict objectForKey:@"hours"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HowWorksViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HowWorksViewController"];
    controller.isBuyingCredits=YES;
    controller.showBackBtn=YES;
    controller.creditInfo=[creditList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
