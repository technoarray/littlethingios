//
//  CreateAppointmentViewController.m
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "CreateAppointmentViewController.h"

@interface CreateAppointmentViewController ()

@end

@implementation CreateAppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _cancelBtn.layer.cornerRadius = 45/2;
    _cancelBtn.clipsToBounds=YES;
    
    _saveBtn.layer.cornerRadius = 45/2;
    _saveBtn.clipsToBounds=YES;
    
    _dateLbl.text = [Helper convertDateForToLbl:_selectedDate];
    
    UIImageView *dropImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage.contentMode = UIViewContentModeCenter;
    dropImage.image = [UIImage imageNamed:@"down-arrow.png"];
    _timeFromTxt.rightView = dropImage;
    _timeFromTxt.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *dropImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage1.contentMode = UIViewContentModeCenter;
    dropImage1.image = [UIImage imageNamed:@"down-arrow.png"];
    _timeToTxt.rightView = dropImage1;
    _timeToTxt.rightViewMode = UITextFieldViewModeAlways;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    if (_selectedAppointment)
    {
        _appointmentWith.text=[_selectedAppointment objectForKey:@"appointment_with"];
        _appointmentDesc.text=[_selectedAppointment objectForKey:@"appointment_descp"];
        _timeFromTxt.text=[_selectedAppointment objectForKey:@"time_from"];
        _timeToTxt.text=[_selectedAppointment objectForKey:@"time_to"];
        _location.text=[_selectedAppointment objectForKey:@"appointment_location"];
        
        NSString *date = [_selectedAppointment objectForKey:@"appointment_date"];
        NSArray *list = [date componentsSeparatedByString:@"-"];
        date = [NSString stringWithFormat:@"%@-%@-%@", [list objectAtIndex:2], [list objectAtIndex:1], [list objectAtIndex:0]];
        startDate = [Helper getDateFromDateString:[NSString stringWithFormat:@"%@ %@",date, _timeFromTxt.text]];
        endDate = [Helper getDateFromDateString:_timeToTxt.text];
        
        if ([[_selectedAppointment objectForKey:@"appointment_descp"] length]>0)
        {
            _appointmentDesc.textColor = [UIColor blackColor];
        }
    }
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender
{
    if (_appointmentWith.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter appointment title."];
    }
    else if (_appointmentDesc.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter appointment info."];
    }
    else if (_timeFromTxt.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please select appointment start time."];
    }
//    else if (_timeToTxt.text.length==0)
//    {
//        [Helper showAlertViewWithTitle:ALERT message:@"Please enter appointment end time."];
//    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:_appointmentWith.text forKey:@"appointment_with"];
        [details setObject:_appointmentDesc.text forKey:@"appointment_descp"];
        [details setObject:[Helper convertDateForAPI:_selectedDate] forKey:@"appointment_date"];
        [details setObject:_timeFromTxt.text forKey:@"time_from"];
        [details setObject:_timeToTxt.text forKey:@"time_to"];
        [details setObject:_location.text forKey:@"location"];
        
        if (_selectedAppointment)
        {
            [details setObject:@"updateEvent" forKey:@"method"];
            [details setObject:[_selectedAppointment objectForKey:@"appointment_id"] forKey:@"id"];
            NSLog(@"%@", details);
            [Helper showIndicatorWithText:@"Updatng..." inView:self.view];
            [api updateAppointmentWithDetails:details];
        }
        else
        {
            [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
            [details setObject:@"addEvent" forKey:@"method"];
            NSLog(@"%@", details);
            [Helper showIndicatorWithText:@"Adding..." inView:self.view];
            [api addAppointmentWithDetails:details];
        }
    }
}

-(void)updateDateField:(UIDatePicker *)picker
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *currentTime = [dateFormatter stringFromDate:picker.date];
    NSLog(@"%@", currentTime);
    if (picker.tag==1)
    {
        _timeFromTxt.text = currentTime;
        startDate = picker.date;
    }
    else
    {
        _timeToTxt.text = currentTime;
        endDate = picker.date;
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackAddAppointmentSuccess:(id)response
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)callBackUpdateAppointmentSuccess:(id)response
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:response];
    [dict setObject:[_selectedAppointment objectForKey:@"appointment_id"] forKey:@"appointment_id"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAppointment" object:dict];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==3 || textField.tag==4)
    {
        UIDatePicker *datePicker = [[UIDatePicker alloc]init];
        datePicker.datePickerMode = UIDatePickerModeTime;
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        if (textField.tag==3)
        {
            datePicker.tag=1;
            if (endDate)
            {
                datePicker.maximumDate = endDate;
            }
            _timeFromTxt.inputView = datePicker;
        }
        else
        {
            datePicker.tag=2;
            if (startDate)
            {
                datePicker.minimumDate = startDate;
            }
            _timeToTxt.inputView = datePicker;
        }
        
        if (self.view.frame.size.width==320)
        {
            activeField = textField;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-60, self.view.frame.size.width, self.view.frame.size.height)];
            
            [UIView commitAnimations];
        }
    }
    if (self.view.frame.size.width==320 && textField.tag==5)
    {
        activeField = textField;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-60, self.view.frame.size.width, self.view.frame.size.height)];
        
        [UIView commitAnimations];
    }
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.view.frame.size.width==320)
    {
        if (textField == activeField)
        {
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
        }
        else
        {
            if (textField.tag==3 || textField.tag==4)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+60, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_appointmentDesc becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- TextView Delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_appointmentDesc.text isEqualToString:@"Notes"])
    {
        _appointmentDesc.text=@"";
        _appointmentDesc.textColor = [UIColor blackColor];
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-60, self.view.frame.size.width, self.view.frame.size.height)];
    
    [UIView commitAnimations];
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (textView.text.length==0)
    {
        textView.text = @"Notes";
        textView.textColor = [UIColor lightGrayColor];
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+60, self.view.frame.size.width, self.view.frame.size.height)];
    
    [UIView commitAnimations];
    
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
