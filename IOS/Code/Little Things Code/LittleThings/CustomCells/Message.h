//
//  Message.h
//  PhoneBox
//
//  Created by Apple on 17/01/17.
//  Copyright © 2017 PhoneBox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
/** String that is used as an identifier for this particular message. */
@property (nonatomic, strong) NSString* messageId;

/** Array of ids of the recipients of the message. */
@property (nonatomic, strong) NSArray* recipientIds;

/** The id of the sender of the message. */
@property (nonatomic, strong) NSString* senderId;

/** Message body text */
@property (nonatomic, strong) NSString* text;

/**
 * Message headers
 *
 * Any application-defined message meta-data
 * can be passed via headers.
 *
 * E.g. a human-readable "display name / username"
 * can be convenient to send as an application-defined
 * header.
 *
 **/
@property (nonatomic, strong) NSDictionary* headers;

/**
 * Message timestamp
 *
 * Server-side-based timestamp for the message.
 * May be nil for message which is created locally, i.e. an outgoing message.
 */
@property (nonatomic, strong) NSDate* timestamp;

@end
