//
//  Message.m
//  PhoneBox
//
//  Created by Apple on 17/01/17.
//  Copyright © 2017 PhoneBox. All rights reserved.
//

#import "Message.h"

@implementation Message
@synthesize messageId, recipientIds, senderId, text, headers, timestamp;

-(id)init{
    self=[super init];
    if (self) {
        messageId=@"";
        senderId=@"";
        text=@"";
    }
    return self;
}

@end
