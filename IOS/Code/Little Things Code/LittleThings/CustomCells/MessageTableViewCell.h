#import <UIKit/UIKit.h>
//#import "AsyncImageView.h"

@interface MessageTableViewCell : UITableViewCell

@property (nonatomic, readwrite, strong) IBOutlet UITextView *message;
@property (nonatomic, readwrite, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, readwrite, strong) IBOutlet UILabel *timestampLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImage;
@property (weak, nonatomic) IBOutlet UIImageView *sentImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonImageLarge;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;

@end
