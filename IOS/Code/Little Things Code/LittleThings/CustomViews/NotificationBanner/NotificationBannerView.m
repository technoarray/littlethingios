//
//  NotificationBannerView.m
//  PhoneBox
//
//  Created by Apple on 06/01/17.
//  Copyright © 2017 PhoneBox. All rights reserved.
//

#import "NotificationBannerView.h"

@implementation NotificationBannerView
@synthesize callBack, messageLbl;


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)designView{
    UIImageView *imgBackgroud=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    imgBackgroud.backgroundColor=[UIColor blackColor];
    imgBackgroud.alpha=0.9;
    imgBackgroud.userInteractionEnabled=YES;
    [self addSubview:imgBackgroud];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lt-small.png"]];
    [imageView setFrame:CGRectMake(10, 30, 60, 60)];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    imageView.userInteractionEnabled=YES;
    imageView.contentMode=UIViewContentModeScaleAspectFill;

    [imgBackgroud addSubview:imageView];
    
    messageLbl=[[UILabel alloc]initWithFrame:CGRectMake(80, 20, self.frame.size.width-90, self.frame.size.height-40)];
    messageLbl.numberOfLines=0;
    messageLbl.textColor=[UIColor whiteColor];
    messageLbl.text=@"This is the sample text.";
    [imgBackgroud addSubview:messageLbl];
    
    
    UIButton *btnCross=[UIButton buttonWithType:UIButtonTypeCustom];
    btnCross.frame=CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    //[btnCross setImage:NSBUNDLE_IMAGE(@"map_icon_close") forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(openChat) forControlEvents:UIControlEventTouchUpInside];
    [imgBackgroud addSubview:btnCross];
    
    [self performSelector:@selector(btnCrossButtonClick) withObject:nil afterDelay:5.0f];
}

-(void)btnCrossButtonClick{
    [self.callBack CancelButtonClick];
}

-(void)openChat{
    [self.callBack openChatScreen];
}

@end
