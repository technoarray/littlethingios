//
//  NotificationBannerView.h
//  PhoneBox
//
//  Created by Apple on 06/01/17.
//  Copyright © 2017 PhoneBox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotificationBannerViewDelegate<NSObject>
-(void)CancelButtonClick;
-(void)openChatScreen;
@end

@interface NotificationBannerView : UIView{

}

@property(nonatomic,weak) id<NotificationBannerViewDelegate>callBack;
@property(nonatomic, strong) UILabel *messageLbl;
-(void)designView;

@end
