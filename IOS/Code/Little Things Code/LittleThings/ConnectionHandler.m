//
//  ConnectionHandler.m
//  browze
//
//  Created by HashBrown Systems on 14/03/14.
//  Copyright (c) 2014 hashbrown. All rights reserved.
//

#import "ConnectionHandler.h"
#import "AppDelegate.h"

@implementation ConnectionHandler
@synthesize manager;

static ConnectionHandler *sharedInstance = nil;

#pragma mark -- Initialization

+(ConnectionHandler *)getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance initializeConnection];
    }
    return sharedInstance;
}

-(void)initializeConnection
{
    if (manager==nil)
    {
        manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
}

#pragma mark -- Cancel Requests

-(void)stopAllRequests
{
    [manager.operationQueue cancelAllOperations];
}

- (void)cancelAllHTTPOperationsWithPath:(NSString *)path
{
    [[manager session] getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        [self cancelTasksInArray:dataTasks withPath:path];
        [self cancelTasksInArray:uploadTasks withPath:path];
        [self cancelTasksInArray:downloadTasks withPath:path];
    }];
}

- (void)cancelTasksInArray:(NSArray *)tasksArray withPath:(NSString *)path
{
    for (NSURLSessionTask *task in tasksArray) {
        NSRange range = [[[[task currentRequest]URL] absoluteString] rangeOfString:path];
        if (range.location != NSNotFound)
        {
            NSLog(@"------ Request canceled --------");
            [task cancel];
        }
    }
}

#pragma mark -- Server Requests

-(void)jsonGETData:(NSString *)url onCompletion:(void(^)(id completed))completion
{
    /*[self initializeConnection];
    url = [NSString stringWithFormat:@"%@%@", SERVER_URL, url];
    
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"AfNetworkResponse %@",responseObject);
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            completion(responseObject);
        }
        else
        {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions
                                                                   error:&error];
            NSLog(@"AfNetworkResponse: %@", json);
            completion(json);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"failer %@",error);
        NSDictionary *dic=error.userInfo;
        if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"])
        {
            completion(error);
        }
        else
        {
            NSLog(@"No response");
        }
    }];*/
}

-(void)jsonPostData:(NSString *)url : (NSDictionary *)params onCompletion:(void(^)(id completed))completion
{
    [self initializeConnection];
    
    url = [NSString stringWithFormat:@"%@%@", SERVER_URL, url];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:nil error:nil];
    
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    
    NSData *data = [Helper getDataFromDictionary:params];
    
    NSLog(@"%@", [Helper getJSONDtringFromData:data]);
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setHTTPBody:data];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            @try {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                     options:kNilOptions
                                                                       error:&error];
                NSLog(@"AfNetworkResponse: %@", json);
                completion(json);
            }
            @catch (NSException *exception) {
                NSLog(@"Exception: %@", exception);
            }
        } else {
            NSLog(@"Error %@",error);
            NSDictionary *dic=error.userInfo;
            if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"])
            {
                completion(error);
            }
            else
            {
                NSLog(@"No response");
            }
        }
    }] resume];
}

-(void)jsonGETStripeData:(NSString *)url onCompletion:(void(^)(id completed))completion
{
    [self initializeConnection];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"Bearer sk_test_c9ORckF7myQyKzQMpWtfz0Dx" forHTTPHeaderField:@"Authorization"];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            @try {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                     options:kNilOptions
                                                                       error:&error];
                NSLog(@"AfNetworkResponse: %@", json);
                completion(json);
            }
            @catch (NSException *exception) {
                NSLog(@"Exception: %@", exception);
            }
        } else {
            NSLog(@"Error %@",error);
            NSDictionary *dic=error.userInfo;
            if (![[dic objectForKey:@"NSLocalizedDescription"] isEqualToString:@"cancelled"])
            {
                completion(error);
            }
            else
            {
                NSLog(@"No response");
            }
        }
    }] resume];
}

@end
