//
//  AppDelegate.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "AppDelegate.h"
////From mobile app
//#define SINCH_KEY @"67e38f88-f2af-427b-a485-7389feb3141c"
//#define SINCH_SECRET @"K9/kB9JiaEe21Z9khQisnw=="
//#define SINCHHOST @"sandbox.sinch.com"

//From Web
#define SINCH_KEY @"862f5439-9994-4cc4-8ac4-83a4752a97f1"
#define SINCH_SECRET @"kjBKU06ZmkucPGcF/HuZew=="
#define SINCHHOST @"sandbox.sinch.com"

//clientapi.sinch.com

////From steddy
//#define SINCH_KEY @"793ea94b-12de-46bf-ac8d-a9fc38ec0ec7"
//#define SINCH_SECRET @"Cu9V7NCUYUG1uPMd/aIDtQ=="
//#define SINCHHOST @"sandbox.sinch.com"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize messageDelegate, notificationBannerView, messageReceived;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //[[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_UJ2JtMwXNxrJvAcxmKtyl31O"];
    
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_live_SDsjG5RFMX7noxXLr9VVLtJn"];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:43/255.0 green:77/255.0 blue:97/255.0 alpha:1]];
    
    self.push = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentAutomatic];
    self.push.delegate = self;
    [self.push setDesiredPushTypeAutomatically];
    [self.push registerUserNotificationSettings];
    
    _messages = [[NSMutableArray alloc]init];
    _showChatScreen=YES;
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        // Code for old versions
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
        [application registerForRemoteNotifications];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"email"])
    {
        //[Helper registerForSinch];
        NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"email"]];
        //        if ([receiverID isEqualToString:@"34"]) {
        //            receiverID=@"admin";
        //        }
        [self initSinchClientWithUserId:receiverID];
        UINavigationController *navConroler = (UINavigationController *)self.window.rootViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"sw"];
        [navConroler setViewControllers:@[controller]];
    }
    
    return YES;
}

- (void)initSinchClientWithUserId:(NSString *)userId
{
    //if (!_client) {
        _client = [Sinch clientWithApplicationKey:SINCH_KEY
                                applicationSecret:SINCH_SECRET
                                  environmentHost:SINCHHOST
                                           userId:userId];
        
        _client.delegate = self;
        [_client messageClient].delegate = self;
        [_client setSupportMessaging:YES];
        [_client setSupportPushNotifications:YES];
        [_client startListeningOnActiveConnection];
        [_client enableManagedPushNotifications];
        [_client setSupportActiveConnectionInBackground:YES];
        [_client start];
    //}
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    _showChatScreen = YES;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -- Push Notification methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [self.push application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *tokenStr = [deviceToken description];
    NSString *pushToken = [[[tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [defaults setObject:pushToken forKey:@"token"];
    [defaults synchronize];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"%@", userInfo);
    //[self.sinch.push application:application didReceiveRemoteNotification:userInfo];
    [self.push application:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@:%@", NSStringFromSelector(_cmd), error);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    //[self handleLocalNotification:notification];
}

- (void)managedPush:(id<SINManagedPush>)unused didReceiveIncomingPushWithPayload:(NSDictionary *)payload
            forType:(NSString *)pushType {
    
}


#pragma mark - SINClientDelegate
- (void)clientDidStart:(id<SINClient>)client {
    NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
    NSLog(@"Sinch client error: %@", [error localizedDescription]);
}

- (void)client:(id<SINClient>)client
    logMessage:(NSString *)message
          area:(NSString *)area
      severity:(SINLogSeverity)severity
     timestamp:(NSDate *)timestamp {
    if (severity == SINLogSeverityCritical) {
        NSLog(@"%@", message);
    }
}

#pragma mark -- Helper Method

-(void)sendMessage:(NSString *)textMessage receiverID:(NSString *)receiverID
{
    SINOutgoingMessage *message = [SINOutgoingMessage messageWithRecipient:[NSString stringWithFormat:@"%@", receiverID] text:textMessage];
    //SINOutgoingMessage *message = [SINOutgoingMessage messageWithRecipient:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]] text:textMessage];
    [self.client messageClient].delegate = self;
    [[self.client messageClient] sendMessage:message];
    
    //    _messageClient.delegate=self;
    //    [_messageClient sendMessage:message];
}

#pragma mark -- SinMessageClient Delegate

- (void) messageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId{
    // Persist outgoing message
    // Update UI
    [_messages addObject:@[ message, @(1) ]];
    NSLog(@"Message Sent");
    if ([messageDelegate respondsToSelector:@selector(dashboardMessageSent:recipientId:)]) {
        [messageDelegate dashboardMessageSent:message recipientId:recipientId];
    }
}

- (void) messageDelivered:(id<SINMessageDeliveryInfo>)info {
    NSLog(@"Message with id %@ was delivered to recipient with id  %@",
          info.messageId,
          info.recipientId);
}

- (void) messageDeliveryFailed:(id<SINMessage>) message info:(NSArray *)messageFailureInfo {
    for (id<SINMessageFailureInfo> reason in messageFailureInfo) {
        NSLog(@"Delivering message with id %@ failed to user %@. Reason %@",
              reason.messageId, reason.recipientId, [reason.error localizedDescription]);
    }
}

-(void) messageFailed:(id<SINMessage>)message info:(id<SINMessageFailureInfo>)messageFailureInfo
{
    NSLog(@"Delivering message with id %@ failed to user %@. Reason %@",
          messageFailureInfo.messageId, messageFailureInfo.recipientId, [messageFailureInfo.error localizedDescription]);
}

- (void)messageClient:(id<SINMessageClient>)messageClient didReceiveIncomingMessage:(id<SINMessage>)message
{
    // Present a Local Notification if app is in background
    
    NSLog(@"message->%@", message.text);
    [_messages addObject:@[ message, @(0) ]];
    if ([messageDelegate respondsToSelector:@selector(dashboardMessageClient:didReceiveIncomingMessage:)]) {
        [messageDelegate dashboardMessageClient:messageClient didReceiveIncomingMessage:message];
    }else{
        messageReceived=message;
        NSLog(@"incoming message->%@", messageReceived.text);
        [self showNotificationBannerWithMessage:message.text];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"Chat" object:nil];
    }
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
        UILocalNotification* notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Message from %@",
                                  [message recipientIds][0]];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    } else {
        // Update UI in-app
    }
}

#pragma mark - Notification banner Method & Delegate

-(void)showNotificationBannerWithMessage:(NSString*)message{
    for (UIView *view in self.window.subviews) {
        if ([view isKindOfClass:[NotificationBannerView class]]) {
            NotificationBannerView *notiView=(NotificationBannerView*)view;
            if (notiView.tag==2001) {
                [notiView removeFromSuperview];
                notiView=nil;
            }
        }
    }
    notificationBannerView=[[NotificationBannerView alloc] initWithFrame:CGRectMake(0, -100, self.window.frame.size.width, 100)];
    notificationBannerView.tag=2001;
    notificationBannerView.callBack=self;
    [notificationBannerView designView];
    notificationBannerView.messageLbl.text=message;
    notificationBannerView.layer.borderWidth=2.0;
    notificationBannerView.layer.borderColor=[UIColor yellowColor].CGColor;
    [self.window addSubview:notificationBannerView];
    
    [UIView animateWithDuration:1.0 animations:^{
        notificationBannerView.alpha=1;
        notificationBannerView.frame=CGRectMake(notificationBannerView.frame.origin.x, 0, notificationBannerView.frame.size.width, notificationBannerView.frame.size.height);
    } completion:^(BOOL finished){
    }];
}

-(void)openChatScreen{
    [UIView animateWithDuration:1.0 animations:^{
        notificationBannerView.alpha=0;
        notificationBannerView.frame=CGRectMake(notificationBannerView.frame.origin.x, -100, notificationBannerView.frame.size.width, notificationBannerView.frame.size.height);
    } completion:^(BOOL finished){
        [notificationBannerView removeFromSuperview];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Chat" object:nil];
    }];
}

-(void)CancelButtonClick{
    [UIView animateWithDuration:1.0 animations:^{
        notificationBannerView.alpha=0;
        notificationBannerView.frame=CGRectMake(notificationBannerView.frame.origin.x, -100, notificationBannerView.frame.size.width, notificationBannerView.frame.size.height);
    } completion:^(BOOL finished){
        [notificationBannerView removeFromSuperview];
    }];
}


@end
