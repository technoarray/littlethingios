//
//  Constants.h
//  browze
//
//  Created by HashBrown Systems on 27/11/14.
//  Copyright (c) 2014 Hashbrown Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

/*********************** BASE URL ****************************/

//#define SERVER_URL @"http://technocodes.us/littlething/web_service/"
#define SERVER_URL @"http://thelittlethingsadmin.com/web_service/"
#define IMAGE_URL @"http://thelittlethingsadmin.com/"

/*********************** COMMON URL ****************************/

//fonts
#define Helvetica_REGULAR @"HelveticaNeue"

//colors
#define APP_COLOR [UIColor colorWithRed:72/255.0f green:148/255.0f blue:235/255.0f alpha:1.0f]
#define APP_COLOR_WITH_ALPHA (alpha) [UIColor colorWithRed:72/255.0f green:148/255.0f blue:235/255.0f alpha:alpha]

//AlertView Titles
#define ALERT @"Alert"
#define OOPS @"Oops"

// AlertView Error Messages
#define INTERNET_ERROR @"Please check your internet connection!"

//constants
#define OK @"OK"

@interface Constants : NSObject

@end
