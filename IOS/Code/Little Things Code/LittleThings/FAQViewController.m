//
//  FAQViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isSearching=NO;
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getFAQQuestions];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackFAQListSuccess:(id)response
{
    faqList = [response objectForKey:@"faq_info"];
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching==YES)
    {
        return searchList.count;
    }
    return faqList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"faqCell"];
    
    NSDictionary *dict;
    if (isSearching==YES)
    {
        dict = [searchList objectAtIndex:indexPath.row];
    }
    else
    {
        dict = [faqList objectAtIndex:indexPath.row];
    }
    
    UILabel *question = [cell viewWithTag:1];
    question.text = [dict objectForKey:@"question"];
    question.numberOfLines=10;
    
    CGRect rect = [Helper calculateHeightForText:question.text fontName:question.font.fontName fontSize:question.font.pointSize maximumWidth:question.frame.size.width];
    if (rect.size.height>20)
    {
        for (NSLayoutConstraint *constraint in question.constraints)
        {
            if ([constraint.identifier isEqualToString:@"faqHeightConstraint"])
            {
                constraint.constant = rect.size.height+5;
                break;
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict;
    if (isSearching==YES)
    {
        dict = [searchList objectAtIndex:indexPath.row];
    }
    else
    {
        dict = [faqList objectAtIndex:indexPath.row];
    }
    FAQAnswerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQAnswerViewController"];
    controller.selectedQuestion = dict;
    [self.navigationController pushViewController:controller animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"faqCell"];
    NSDictionary *dict;
    if (isSearching==YES)
    {
        dict = [searchList objectAtIndex:indexPath.row];
    }
    else
    {
        dict = [faqList objectAtIndex:indexPath.row];
    }
    
    UILabel *question = [cell viewWithTag:1];
    
    float height = 56;
    CGRect rect = [Helper calculateHeightForText:[dict objectForKey:@"question"] fontName:question.font.fontName fontSize:question.font.pointSize maximumWidth:question.frame.size.width];
    if (rect.size.height>20)
    {
        for (NSLayoutConstraint *constraint in question.constraints)
        {
            if ([constraint.identifier isEqualToString:@"faqHeightConstraint"])
            {
                height = 35+rect.size.height+5;
                break;
            }
        }
    }
    return height;
}

#pragma mark -- SearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton=YES;
    isSearching=YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question CONTAINS[cd] %@", searchText];
    searchList = [faqList filteredArrayUsingPredicate:predicate];
    [_table reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isSearching=NO;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [_table reloadData];
    searchBar.showsCancelButton=NO;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
