//
//  BuyCreditsViewController.h
//  LittleThings
//
//  Created by Saurav on 31/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HowWorksViewController.h"

@interface BuyCreditsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *creditList;
}

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property BOOL showBackBtn;
- (IBAction)back:(id)sender;
@end
