//
//  SideMenuViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "DrawerCell.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Helper.h"
#import "AppDelegate.h"
#import <UIImageView+WebCache.h>
#import "WebAPI.h"

@interface SideMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UIAlertViewDelegate, APIsDelegate>
{
    NSArray *list;
    NSArray *iconList;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UIImageView *coverImage;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UITableView *table;
@end
