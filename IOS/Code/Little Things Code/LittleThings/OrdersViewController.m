//
//  OrdersViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "OrdersViewController.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    orderList = [[NSMutableArray alloc]init];
    
    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    api=[[WebAPI alloc]init];
    api.delegate=self;
    
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getMyOrders];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InvoicePaid" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invoicePaid:) name:@"InvoicePaid" object:nil];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackMyOrdersSuccess:(id)response
{
    orderList = [[response objectForKey:@"user_orderhistory"] mutableCopy];
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderCell"];
    //orderDescriptionHeight
    
    UILabel *title = [cell viewWithTag:1];
    UILabel *description = [cell viewWithTag:2];
    UILabel *price = [cell viewWithTag:3];
    UILabel *order = [cell viewWithTag:4];
    UILabel *time = [cell viewWithTag:5];
    UILabel *status = [cell viewWithTag:6];//
    UIButton *receiptBtn = [cell viewWithTag:7];
    [receiptBtn addTarget:self action:@selector(showReceipt:) forControlEvents:UIControlEventTouchUpInside];
    receiptBtn.tag=indexPath.row;
    
    NSDictionary *dict = [orderList objectAtIndex:indexPath.row];
    
    if ([[dict objectForKey:@"pdf_link"] length]>0)
    {
        for (NSLayoutConstraint *constraint in receiptBtn.constraints)
        {
            if ([constraint.identifier isEqualToString:@"receiptHeight"])
            {
                constraint.constant=30;
                break;
            }
        }
    }
    else
    {
        for (NSLayoutConstraint *constraint in receiptBtn.constraints)
        {
            if ([constraint.identifier isEqualToString:@"receiptHeight"])
            {
                constraint.constant=0;
                break;
            }
        }
    }
    
    title.text=[dict objectForKey:@"title"];
    description.text=[dict objectForKey:@"description"];
    
    if ([[dict objectForKey:@"type"] isEqualToString:@"credit buy"])
    {
        price.text=[NSString stringWithFormat:@"%@ Credit(s)", [dict objectForKey:@"credit"]];//for $%@  , [dict objectForKey:@"amount"]
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"credit(s)"])
    {
        price.text=[NSString stringWithFormat:@"%@ Credit(s)", [dict objectForKey:@"amount"]];//for $%@  , [dict objectForKey:@"amount"]
    }
    else
    {
        price.text=[NSString stringWithFormat:@"$%@", [dict objectForKey:@"amount"]];
    }
    
    order.text=[NSString stringWithFormat:@"Order No %@", [dict objectForKey:@"order_id"]];
    time.text = [dict objectForKey:@"modify_date"];
    
    if ([[dict objectForKey:@"status"] isEqualToString:@"Pending"])
    {
        status.hidden=NO;
        status.text=@"Pending";
    }
    else
    {
        status.hidden=YES;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [orderList objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"pdf_link"] length]>0)
    {
        return 122;
    }
    else
    {
        return 90;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [orderList objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"status"] isEqualToString:@"Pending"])
    {
        PaymentDetailsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentDetailsViewController"];
        controller.isPayingForInvoice=YES;
        controller.selectedCredit=dict;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)search:(id)sender {
}

-(void)showReceipt:(UIButton *)sender
{
    NSDictionary *dict = [orderList objectAtIndex:sender.tag];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [dict objectForKey:@"pdf_link"]]]];
}

#pragma mark -- Notification Methods

-(void)invoicePaid:(NSNotification *)notification
{
    long index = [orderList indexOfObject:notification.object];
    NSMutableDictionary *dict = [[orderList objectAtIndex:index] mutableCopy];
    [dict setObject:@"Paid" forKey:@"status"];
    [orderList replaceObjectAtIndex:index withObject:dict];
    [_table reloadData];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
