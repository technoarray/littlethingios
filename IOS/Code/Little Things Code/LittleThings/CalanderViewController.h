//
//  CalanderViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <FSCalendar.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "AppointmentsViewController.h"

@interface CalanderViewController : UIViewController<FSCalendarDelegate, FSCalendarDataSource, APIsDelegate>
{
    WebAPI *api;
    NSArray *events;
    NSDateFormatter *dateFormatter;
}

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet FSCalendar *calendarView;

@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *calendarHeight;

- (IBAction)backDay:(id)sender;
- (IBAction)nextDay:(id)sender;
@end
