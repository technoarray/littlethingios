//
//  DrawerCell.m
//  canoe
//
//  Created by HashBrown Systems on 26/11/14.
//  Copyright (c) 2014 Hashbrown Systems. All rights reserved.
//

#import "DrawerCell.h"

@implementation DrawerCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
