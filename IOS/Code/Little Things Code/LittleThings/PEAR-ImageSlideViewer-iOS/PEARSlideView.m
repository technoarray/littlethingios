 //
//  PEARMainSlideView.m
//  ImageSlideViewer
//
//  Created by hirokiumatani on 2015/12/01.
//  Copyright © 2015年 hirokiumatani. All rights reserved.
//

#import "PEARSlideView.h"

@implementation PEARSlideView

- (id)init
{
    if (self=[super init])
    {
        NSString *nibName = NSStringFromClass([self class]);
        UINib *nib = [UINib nibWithNibName:nibName bundle:nil];
        self = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
        self.frame = [[UIScreen mainScreen] bounds];
        self.alpha = 0.0;
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
        return self;
    }
    return self;
}

- (IBAction)tapCloseButton:(UIButton *)sender
{
    [_delegate tapCloseButton];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger selectedfileFinder = scrollView.contentOffset.x/self.frame.size.width;
    self.pageControl.currentPage = selectedfileFinder;
    if (self.pageControl.currentPage==0) {
    }else{
    }
}

- (IBAction)moreOptionAction:(UIButton *)sender {
    NSString *actionSheetTitle = @"Steddy"; //Action Sheet Title
    NSString *destructiveTitle = @"Delete"; //Action Sheet Button Titles
    NSString *other1 = @"Make a Profile pic";
    //NSString *other2 = @"Delete";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:destructiveTitle
                                  otherButtonTitles:other1, nil];
    actionSheet.tag=102;
    [actionSheet showInView:self];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
//            switch (buttonIndex) {
//                case 0:
//                    [_delegate tapMoreOptions:buttonIndex];
//                    //[self FBShare];
//                    break;
//                case 1:
//                    [_delegate tapMoreOptions:buttonIndex];
//                    break;
//                case 2:
//                   [_delegate tapMoreOptions:buttonIndex];
//                    break;
//                case 3:
//                   [_delegate tapMoreOptions:buttonIndex];
//                    break;
//                case 4:
//                   [_delegate tapMoreOptions:buttonIndex];
//                    break;
//                default:
//                    break;
//            }
}

@end
