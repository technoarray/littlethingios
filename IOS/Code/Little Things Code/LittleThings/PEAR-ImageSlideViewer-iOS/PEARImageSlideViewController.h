//
//  PEARSlideImageViewController.h
//  ImageSlideViewer
//
//  Created by hirokiumatani on 2015/12/01.
//  Copyright © 2015年 hirokiumatani. All rights reserved.
//

#import <UIKit/UIKit.h>
// view
#import "PEARSlideView.h"
#import "PEARZoomView.h"
// util
#import "PEARUtility.h"
#import "UIImageView+WebCache.h"
#import "UIView+WebCacheOperation.h"

/**
 UIWindow(window)
    
    └ PEARSlideView(scrollView)
        └ PEARSlideView(contentView)
            
            └ PEARZoomView(scrollView)
                └ PEARZoomView(imageView)
            
            └ PEARZoomView(scrollView)
                └ PEARZoomView(imageView)
 */

@interface PEARImageSlideViewController : UIViewController<UIScrollViewDelegate,PEARSlideViewDelegate>{

}
@property (strong, nonatomic) NSMutableArray *imageArray;
@property (strong, nonatomic) NSMutableArray *imageIDArray;

- (void)showAtIndex:(NSInteger)index;
- (void)setImageLists:(NSArray *)imageLists;
@end
