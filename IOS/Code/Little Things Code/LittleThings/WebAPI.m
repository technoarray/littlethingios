//
//  WebAPI.m
//  browze
//
//  Created by HashBrown Systems on 04/05/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import "WebAPI.h"

@implementation WebAPI
@synthesize delegate;

#pragma mark -- Authentication Methods

-(void)signupUserWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"signup.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper saveAllValuesToUserDefault:completed];
                    [[self delegate] callBackSignUpUserSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)loginUserWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"login.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper saveAllValuesToUserDefault:completed];
                    [[self delegate] callBackLoginSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)forgotPasswordWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"forgetpassword.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Password" message:[completed objectForKey:@"message"]];
                    [[self delegate] callBackForgotPasswordSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)updateProfileWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"update_user_details.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    [defaults setObject:[details objectForKey:@"name"] forKey:@"name"];
                    [defaults setObject:[details objectForKey:@"gender"] forKey:@"gender"];
                    [defaults setObject:[details objectForKey:@"phone_number"] forKey:@"phone_number"];
                    [defaults setObject:[details objectForKey:@"address"] forKey:@"address"];
                    [defaults setObject:[details objectForKey:@"city"] forKey:@"city"];
                    [defaults setObject:[details objectForKey:@"state"] forKey:@"state"];
                    [defaults setObject:[details objectForKey:@"zipcode"] forKey:@"zipcode"];
                    [defaults setObject:[details objectForKey:@"profile_image"] forKey:@"image"];
                    [defaults synchronize];
                    [[self delegate] callBackUpdateProfileSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)addAppointmentWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Added" message:[completed objectForKey:@"message"]];
                    [[self delegate] callBackAddAppointmentSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)updateAppointmentWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Added" message:[completed objectForKey:@"message"]];
                    [[self delegate] callBackUpdateAppointmentSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getAllAppointments
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"listEventDate" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackAppointmentListSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getAllAppointmentsForDate:(NSDate *)selectedDate
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"listEventDate" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    [details setObject:[Helper convertDateForAPI:selectedDate] forKey:@"date"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackAppointmentListSuccess:completed];
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)deleteAppointment:(NSString *)appointmentID
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"deleteEvent" forKey:@"method"];
    [details setObject:appointmentID forKey:@"id"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Success" message:@"Appointment deleted successfully"];
                    [[self delegate] callBackDeleteAppointmentSuccess:appointmentID];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getAllAppointmentsForUser
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"listEvent" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackAppointmentsForMonthSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getDiscoverCategories
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"discover_cat" forKey:@"method"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackDiscoverCategoriesSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getDiscoverSubcategoriesForCategoryID:(NSString *)categoryID
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"discover_subcat" forKey:@"method"];
    [details setObject:categoryID forKey:@"cat_id"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackDiscoverCategoriesSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getDiscoverTextWithSubcategoryID:(NSString *)subcategoryID
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"discover_subcatDescp" forKey:@"method"];
    [details setObject:subcategoryID forKey:@"id"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackDiscoverSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)saveCardWithToken:(NSString *)token
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"saveCredit" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    [details setObject:token forKey:@"token"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackSaveCardSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)buyCreditsWithDetails:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:[completed objectForKey:@"total_credit"] forKey:@"credit"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [Helper showAlertViewWithTitle:@"Success" message:@"Credits added successfully."];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)buyCreditsWithDetails:(NSMutableDictionary *)details cardParams:(STPCardParams *)cardParams
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:[completed objectForKey:@"total_credit"] forKey:@"credit"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [Helper showAlertViewWithTitle:@"Success" message:@"Credits added successfully."];
                    [[self delegate] callBackBuyCreditsSuccess:cardParams];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)applyCouponCode:(NSString *)couponCode
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"couponCode" forKey:@"method"];
    [details setObject:couponCode forKey:@"coupon_code"];
    
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Success" message:[NSString stringWithFormat:@"Coupon code applied successfully. You will get %@%% discount.", [completed objectForKey:@"discount"]]];
                    [[self delegate] callBackApplyCouponSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)payForInvoceWithDetails:(NSMutableDictionary *)details cardParams:(STPCardParams *)cardParams
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [Helper showAlertViewWithTitle:@"Success" message:@"Payment done successfully."];
                    [[self delegate] callBackBuyCreditsSuccess:cardParams];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getMyCredits
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"showCredit" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:[completed objectForKey:@"credit"] forKey:@"credit"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[self delegate] callBackMyCreditsSuccess:completed];
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                //[Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            //[Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getUserHistory
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"listOrderHistory" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackHistorySuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getUserInVoices
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"listInvoices" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackInvoicesSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getMyOrders
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"myorder" forKey:@"method"];
    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                [[self delegate] callBackMyOrdersSuccess:completed];
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getFAQQuestions
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"faq_qus" forKey:@"method"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackFAQListSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getFAQAnswerForQuestion:(NSString *)faqID
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    [details setObject:@"faq_ans" forKey:@"method"];
    [details setObject:faqID forKey:@"id"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callBackFAQResponseSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

#pragma mark -- Stripe Methods

-(void)getStripeUserDetails:(NSString *)stripeID
{
    NSString *requestURL=[NSString stringWithFormat:@"https://api.stripe.com/v1/customers/%@", stripeID];
    NSLog(@"%@%@", SERVER_URL, requestURL);
    
    [[ConnectionHandler getSharedInstance] jsonGETStripeData:requestURL onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([completed objectForKey:@"id"])
                {
                    [[self delegate] callBackStripeUserDetailsSuccess:completed];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)submitTokenToBackend:(NSDictionary *)details Completion:(void(^)(id completed))completion
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);//[api payForInvoceWithDetails:details cardParams:cardParams];
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try
        {
            completion (completed);
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            completion(exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally
        {
            completion(nil);
        }
    }];
}

#pragma mark - get chat message
-(void)getChatHistory:(NSMutableDictionary *)details
{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([completed objectForKey:@"chat_users"] != nil)
                {
                    [[self delegate] callbackGetChatMessages:completed];
                }
                //                else
                //                {
                //                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                //                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)saveMessage:(NSMutableDictionary *)details{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callbackFromAPI:completed];
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)saveImageMessage:(NSMutableDictionary *)details{
    NSString *signUpURL=[NSString stringWithFormat:@"index.php"];
    NSLog(@"%@", details);
    
    [[ConnectionHandler getSharedInstance] jsonPostData:signUpURL :details onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"code"] integerValue]==1)
                {
                    [[self delegate] callbackFromSaveImageMessage:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}



@end
