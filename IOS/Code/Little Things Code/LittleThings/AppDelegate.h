//
//  AppDelegate.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <Stripe.h>
#import "Helper.h"
#import "SinchVideoHelper.h"
#import <Sinch/Sinch.h>
#import "NotificationBannerView.h"

@protocol IncomingMessageDelegate <NSObject>
@optional
#pragma mark - SINMessageClientDelegate
- (void)dashboardMessageClient:(id<SINMessageClient>)messageClient didReceiveIncomingMessage:(id<SINMessage>)message;
- (void)dashboardMessageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId;
@end

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, SINManagedPushDelegate, SINMessageClientDelegate, SINClientDelegate, NotificationBannerViewDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readwrite, strong) id<SINManagedPush> push;
@property (strong, nonatomic) id<SINMessageClient> messageClient;
@property (strong, nonatomic) id<SINClient> client;
@property (strong, nonatomic) id<SINMessage> messageReceived;
@property(nonatomic, retain) id<IncomingMessageDelegate> messageDelegate;
@property (strong, nonatomic) NotificationBannerView *notificationBannerView;
@property (assign) BOOL showChatScreen;
@property (strong, nonatomic) NSMutableArray *messages;
-(void)sendMessage:(NSString *)textMessage receiverID:(NSString *)receiverID;
- (void)initSinchClientWithUserId:(NSString *)userId;
-(void)openChatScreen;
-(void)CancelButtonClick;
@end

