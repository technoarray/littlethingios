//
//  AppointmentDetailViewController.h
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "WebAPI.h"
#import "CreateAppointmentViewController.h"

@interface AppointmentDetailViewController : UIViewController<UIAlertViewDelegate, APIsDelegate>
{
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UILabel *message;
@property (strong, nonatomic) IBOutlet UIButton *appointmentBtn;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *appointmentName;
@property (strong, nonatomic) IBOutlet UILabel *info;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;

@property (strong, nonatomic) NSDictionary *details;
@property (strong, nonatomic) NSDate *selectedDate;

- (IBAction)back:(id)sender;
- (IBAction)addAppointment:(id)sender;
- (IBAction)edit:(id)sender;
- (IBAction)remove:(id)sender;

@end
