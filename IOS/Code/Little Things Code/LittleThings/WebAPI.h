//
//  WebAPI.h
//  browze
//
//  Created by HashBrown Systems on 04/05/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "ConnectionHandler.h"
#import "Constants.h"
#import "WebAPI.h"
#import <Stripe.h>

@protocol APIsDelegate <NSObject>

@optional
- (void) callbackFromAPI:(id)response;

//user methods
-(void) callBackLoginSuccess:(id)response;
-(void) callBackSignUpUserSuccess:(id)response;
-(void) callBackForgotPasswordSuccess:(id)response;
-(void) callBackUpdateProfileSuccess:(id)response;
-(void) callBackAddAppointmentSuccess:(id)response;
-(void) callBackUpdateAppointmentSuccess:(id)response;
-(void) callBackAppointmentListSuccess:(id)response;
-(void) callBackDeleteAppointmentSuccess:(id)response;
-(void) callBackSaveCardSuccess:(id)response;
-(void) callBackBuyCreditsSuccess:(STPCardParams *)response;
-(void) callBackMyCreditsSuccess:(id)response;
-(void) callBackFAQListSuccess:(id)response;
-(void) callBackFAQResponseSuccess:(id)response;
-(void) callBackHistorySuccess:(id)response;
-(void) callBackInvoicesSuccess:(id)response;
-(void) callBackMyOrdersSuccess:(id)response;
-(void) callBackAppointmentsForMonthSuccess:(id)response;
-(void) callBackDiscoverCategoriesSuccess:(id)response;
-(void) callBackDiscoverSuccess:(id)response;
-(void) callBackApplyCouponSuccess:(id)response;

//stripe methods
-(void) callBackStripeUserDetailsSuccess:(id)response;

//get chat messages
- (void) callbackGetChatMessages:(id)response;

- (void) callbackFromSaveImageMessage:(id)response;

@end

@interface WebAPI : NSObject
{
    __weak id <APIsDelegate> delegate;
}

@property (nonatomic,weak) id delegate;

//user methods
-(void)signupUserWithDetails:(NSMutableDictionary *)details;
-(void)loginUserWithDetails:(NSMutableDictionary *)details;
-(void)forgotPasswordWithDetails:(NSMutableDictionary *)details;
-(void)updateProfileWithDetails:(NSMutableDictionary *)details;

-(void)addAppointmentWithDetails:(NSMutableDictionary *)details;
-(void)updateAppointmentWithDetails:(NSMutableDictionary *)details;
-(void)getAllAppointments;
-(void)getAllAppointmentsForDate:(NSDate *)selectedDate;
-(void)deleteAppointment:(NSString *)appointmentID;
-(void)getAllAppointmentsForUser;

-(void)getDiscoverCategories;
-(void)getDiscoverSubcategoriesForCategoryID:(NSString *)categoryID;
-(void)getDiscoverTextWithSubcategoryID:(NSString *)subcategoryID;

-(void)saveCardWithToken:(NSString *)token;
-(void)buyCreditsWithDetails:(NSMutableDictionary *)details;
-(void)buyCreditsWithDetails:(NSMutableDictionary *)details cardParams:(STPCardParams *)cardParams;
-(void)getMyCredits;
-(void)applyCouponCode:(NSString *)couponCode;

-(void)payForInvoceWithDetails:(NSMutableDictionary *)details cardParams:(STPCardParams *)cardParams;

-(void)getFAQQuestions;
-(void)getFAQAnswerForQuestion:(NSString *)faqID;

-(void)getUserHistory;
-(void)getUserInVoices;
-(void)getMyOrders;

-(void)loginUserWithCode:(NSString *)countryCode phone:(NSString *)phone;
-(void)verifyUserWithDetails:(NSMutableDictionary *)details;

-(void)submitTokenToBackend:(NSDictionary *)details Completion:(void(^)(id completed))completion;

// Stripe Methods
-(void)getStripeUserDetails:(NSString *)stripeID;

//Get Chat History
-(void)getChatHistory:(NSMutableDictionary *)details;
-(void)saveMessage:(NSMutableDictionary *)details;
-(void)saveImageMessage:(NSMutableDictionary *)details;

@end
