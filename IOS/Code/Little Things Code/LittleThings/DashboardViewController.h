//
//  DashboardViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "SinchVideoHelper.h"
#import "MessageTableViewCell.h"
#import "Message.h"
#import "UIImageView+WebCache.h"
#import "objc/runtime.h"
#import "UIView+WebCacheOperation.h"
#import "PEARImageSlideViewController.h"

@interface DashboardViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UITextViewDelegate, UIAlertViewDelegate, APIsDelegate, IncomingMessageDelegate, UIActionSheetDelegate>
{
    WebAPI *api;
    AppDelegate *appDelegate;
    UIRefreshControl *refreshControl;
    int pageNo;
    int totalPage;
    float keyboard;
}

@property (nonatomic,retain)PEARImageSlideViewController * slideImageViewController;
@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) IBOutlet UITextView *messageField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraintHeight;

@property (strong, nonatomic) UIImage *cameraImage;
@property (strong, nonatomic) NSMutableArray *messages;
- (IBAction)sendMessage:(id)sender;
- (IBAction)selectImage:(id)sender;
- (void)scrollToBottom;
@end
