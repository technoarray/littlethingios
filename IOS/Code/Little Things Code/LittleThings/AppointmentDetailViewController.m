//
//  AppointmentDetailViewController.m
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "AppointmentDetailViewController.h"

@interface AppointmentDetailViewController ()

@end

@implementation AppointmentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateContent];
    
    _appointmentBtn.layer.cornerRadius = 50/2;
    _appointmentBtn.clipsToBounds = YES;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateAppointment" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAppointment:) name:@"UpdateAppointment" object:nil];
}

#pragma mark -- button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addAppointment:(id)sender
{
    CreateAppointmentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateAppointmentViewController"];
    controller.selectedDate=_selectedDate;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)edit:(id)sender
{
    CreateAppointmentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateAppointmentViewController"];
    controller.selectedAppointment = _details;
    controller.selectedDate=_selectedDate;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)remove:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete Appointment" message:@"Do you really want to delete this appointment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag=1;
    [alert show];
}

#pragma mark -- Helper Methods

-(void)updateContent
{
    _dateLbl.text = [NSString stringWithFormat:@"%@ at %@", [Helper convertDateForToLbl:_selectedDate], [_details objectForKey:@"time_from"]];
    _name.text = [_details objectForKey:@"appointment_with"];
    
    if ([[_details objectForKey:@"appointment_descp"] isEqualToString:@"Notes"])
    {
        _message.text = @"";
    }
    else
    {
        _message.text = [_details objectForKey:@"appointment_descp"];
    }
    
    _appointmentName.text = [_details objectForKey:@"appointment_with"];
    _time.text = [_details objectForKey:@"time_diff"];
    
    if ([[_details objectForKey:@"appointment_descp"] isEqualToString:@"Notes"])
    {
        _info.text = @"";
    }
    else
    {
        _info.text = [_details objectForKey:@"appointment_descp"];
    }
    
    CGRect rect = [Helper calculateHeightForText:_info.text fontName:_info.font.fontName fontSize:_info.font.pointSize maximumWidth:self.view.frame.size.width-51];
    _viewHeight.constant = rect.size.height+66;
}

#pragma mark -- Notification Methods

-(void)updateAppointment:(NSNotification *)notification
{
    _details = notification.object;
    [self updateContent];
}

#pragma mark -- AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            if ([Helper isInternetConnected]==YES)
            {
                [Helper showIndicatorWithText:@"Deleting..." inView:self.view];
                [api deleteAppointment:[_details objectForKey:@"appointment_id"]];
            }
            else
            {
                [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
            }
        }
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackDeleteAppointmentSuccess:(id)response
{
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteAppointment" object:response];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
