//
//  DiscoverViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "DiscoverViewController.h"

@interface DiscoverViewController ()

@end

@implementation DiscoverViewController

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"appear");
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        if (_discoverMessage.text.length==0)
        {
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        }
        [api getDiscoverTextWithSubcategoryID:[_selectedSubcategory objectForKey:@"sub_cat_id"]];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_sideMenu addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    _name.text = [_selectedSubcategory objectForKey:@"sub_cat_name"];
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[_selectedSubcategory objectForKey:@"sub_cat_image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [_indicator stopAnimating];
         _imageView.image = image;
     }];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackDiscoverSuccess:(id)response
{
    NSDictionary *dict = [[response objectForKey:@"discover_info"] firstObject];
    _discoverMessage.text = [dict objectForKey:@"sub_cat_description"];
}

#pragma mark -- Action Methods

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
