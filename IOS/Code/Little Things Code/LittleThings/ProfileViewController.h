//
//  ProfileViewController.h
//  LittleThings
//
//  Created by Saurav on 17/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <UIImageView+WebCache.h>

@interface ProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIButton *messageBtn;

- (IBAction)back:(id)sender;
- (IBAction)message:(id)sender;
- (IBAction)editProfile:(id)sender;
@end
