//
//  PaymentDetailsViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMNumberKeyboard/MMNumberKeyboard.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import <Stripe.h>
#import "SWRevealViewController.h"

@interface PaymentDetailsViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, APIsDelegate, UIAlertViewDelegate>
{
    UITextField *activeField;
    NSArray *monthList, *yearList, *cardList;
    WebAPI *api;
    long selectedIndex;
}

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UITextField *cardNumber;
@property (strong, nonatomic) IBOutlet UITextField *expiryMonth;
@property (strong, nonatomic) IBOutlet UITextField *expiryYear;
@property (strong, nonatomic) IBOutlet UITextField *securityCode;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIView *cardView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *monthWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageXCoordinateConstraint;
@property (strong, nonatomic) IBOutlet UIView *savedCardView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;


@property BOOL isBuyingCredits;
@property BOOL isPayingForInvoice;
@property BOOL showBackBtn;
@property (strong, nonatomic) NSDictionary *selectedCredit;
@property (strong, nonatomic) NSDictionary *discountDetails;

@property (strong, nonatomic) IBOutlet UIView *firstDot;
@property (strong, nonatomic) IBOutlet UIView *secondDot;
@property (strong, nonatomic) IBOutlet UIView *thirdDot;
@property (strong, nonatomic) IBOutlet UIView *fourthDot;
@property (strong, nonatomic) IBOutlet UIView *fifthDot;
@property (strong, nonatomic) IBOutlet UIView *sixthDot;
@property (strong, nonatomic) IBOutlet UIView *seventhDot;
@property (strong, nonatomic) IBOutlet UIView *eigthDot;
@property (strong, nonatomic) IBOutlet UIView *ninthDot;
@property (strong, nonatomic) IBOutlet UIView *tenthDot;
@property (strong, nonatomic) IBOutlet UIView *eleventhDot;
@property (strong, nonatomic) IBOutlet UIView *twelthDot;
@property (strong, nonatomic) IBOutlet PKPaymentButton *applePayBtn;
@property (strong, nonatomic) IBOutlet UIView *scrollContentView;

@property (strong, nonatomic) IBOutlet UITextField *couponTxt;


- (IBAction)back:(id)sender;
- (IBAction)submit:(id)sender;
- (IBAction)applyCoupon:(id)sender;
@end
