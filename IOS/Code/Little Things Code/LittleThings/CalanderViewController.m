//
//  CalanderViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "CalanderViewController.h"

@interface CalanderViewController ()

@end

@implementation CalanderViewController

-(void)viewWillAppear:(BOOL)animated
{
    if ([Helper isInternetConnected]==YES)
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getAllAppointmentsForUser];
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _calendarView.appearance.headerMinimumDissolvedAlpha = 0;
    _calendarView.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesDefaultCase;
    _calendarView.calendarHeaderView.backgroundColor = [Helper colorFromHexString:@"CD8181"];
    _calendarView.calendarWeekdayView.backgroundColor = [Helper colorFromHexString:@"CD8181"];
    _calendarView.appearance.headerTitleFont=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:22];
    _calendarView.appearance.titleFont=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:17];
    _calendarView.appearance.subtitleFont=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:17];
    _calendarView.appearance.weekdayFont=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:17];
    _calendarView.appearance.headerTitleColor=[UIColor blackColor];
    _calendarView.appearance.weekdayTextColor=[UIColor whiteColor];
    
    _calendarView.appearance.selectionColor = [Helper colorFromHexString:@"2B4D61"];
    _calendarView.appearance.todaySelectionColor = [Helper colorFromHexString:@"2B4D61"];
    _calendarView.appearance.todayColor = [Helper colorFromHexString:@"2B4D61"];
    
    NSLog(@"%@", self.calendarView.currentPage);
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Chat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openChat) name:@"Chat" object:nil];
}

#pragma mark -- FSCalendar Delegate

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarHeight.constant = CGRectGetHeight(bounds);
    // Do other updates here
    [self.view layoutIfNeeded];
}

-(void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSLog(@"%@", date);
    AppointmentsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AppointmentsViewController"];
    controller.hidesBottomBarWhenPushed = YES;
    controller.selectedDate=date;
    [self.navigationController pushViewController:controller animated:YES];
}

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    NSString *dateString = [dateFormatter stringFromDate:date];
    int count = 0;
    for (NSDictionary *dict in events)
    {
        if ([[dict objectForKey:@"appointment_date"] isEqualToString:dateString])
        {
            count = count+1;
        }
    }
    return count;
}

#pragma mark -- Button Action

- (IBAction)backDay:(id)sender
{
    NSDate *currentMonth = self.calendarView.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendarView setCurrentPage:previousMonth animated:YES];
    
    
    NSLog(@"%@", self.calendarView.currentPage);
}

- (IBAction)nextDay:(id)sender
{
    NSDate *currentMonth = self.calendarView.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendarView setCurrentPage:nextMonth animated:YES];
    
    NSLog(@"%@", self.calendarView.currentPage);
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackAppointmentsForMonthSuccess:(id)response
{
    if ([[response objectForKey:@"code"] integerValue]==1)
    {
        events = [response objectForKey:@"user_events"];
        [self.calendarView reloadData];
    }
}

#pragma mark -- Notification Methods

-(void)openChat
{
    if (self.tabBarController.selectedIndex != 1) {
        self.tabBarController.selectedIndex=1;
    }
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
