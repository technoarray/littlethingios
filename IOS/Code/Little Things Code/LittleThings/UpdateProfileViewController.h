//
//  UpdateProfileViewController.h
//  LittleThings
//
//  Created by Saurav on 29/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import <MMNumberKeyboard/MMNumberKeyboard.h>
#import <UIImageView+WebCache.h>

@interface UpdateProfileViewController : UIViewController<UITextFieldDelegate, APIsDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    WebAPI *api;
    UITextField *activeField;
    NSString *imageString;
}

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *gender;
@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UITextField *state;
@property (strong, nonatomic) IBOutlet UITextField *zip;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *imageBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageBtnXCoordinate;

- (IBAction)back:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)changeImage:(id)sender;
@end
