//
//  FAQAnswerViewController.m
//  LittleThings
//
//  Created by Saurav on 04/04/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "FAQAnswerViewController.h"

@interface FAQAnswerViewController ()

@end

@implementation FAQAnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    _question.text = [_selectedQuestion objectForKey:@"question"];
    _question.numberOfLines=10;
    
    CGRect rect = [Helper calculateHeightForText:_question.text fontName:_question.font.fontName fontSize:_question.font.pointSize maximumWidth:self.view.frame.size.width-32];
    if (rect.size.height>20)
    {
        _questionHeight.constant = rect.size.height+5;
    }
    
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getFAQAnswerForQuestion:[_selectedQuestion objectForKey:@"id"]];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackFAQResponseSuccess:(id)response
{
    NSDictionary *dict = [[response objectForKey:@"faq_info"] firstObject];
    _answer.text = [dict objectForKey:@"answer"];
}

#pragma mark -- button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
