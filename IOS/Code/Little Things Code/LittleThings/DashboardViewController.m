//
//  DashboardViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "DashboardViewController.h"

typedef NS_ENUM(int, MessageDirection) {
    Incoming,
    Outgoing,
};

@interface DashboardViewController (){
    NSDateFormatter* _dateFormatter;
}

@end

@implementation DashboardViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _messageField.userInteractionEnabled=YES;
    _sendBtn.userInteractionEnabled=YES;
    appDelegate.messageDelegate=self;
    
    //_messageField.font = [UIFont fontWithName:@"AWConquerorDidot-Light" size:15];
    _messageField.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    
    [self.table reloadData];
    [self scrollToBottom];
    /*[self.table addObserver:self forKeyPath:@"contentOffset"
     options:NSKeyValueObservingOptionNew
     context:NULL];
     [self.table addObserver:self forKeyPath:@"contentSize"
     options:NSKeyValueObservingOptionNew
     context:NULL];*/
    
    /*if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"credit"] floatValue]<20)
     {
     _messageField.userInteractionEnabled=NO;
     _sendBtn.userInteractionEnabled=NO;
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buy Credits" message:@"You don't have minimum credits to use this feature. Kindly buy some credits." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Buy", nil];
     alert.tag=1;
     [alert show];
     }
     else
     {
     [api getUserInVoices];
     _messageField.userInteractionEnabled=YES;
     _sendBtn.userInteractionEnabled=YES;
     }*/
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    appDelegate.messageDelegate=nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //_messages = [[NSMutableArray alloc]init];
    pageNo = 0;
    totalPage=1;
    [self registerForKeyboardNotifications];
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    
    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _sendBtn.layer.cornerRadius = 40/2;
    _sendBtn.clipsToBounds = YES;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OpenRequest" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCalendar) name:@"OpenRequest" object:nil];//
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"hideKeyboard" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard) name:@"hideKeyboard" object:nil];
    
    [self performSelector:@selector(getChatHistory) withObject:nil afterDelay:0.1f];
    
    // Refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.tag = 100;
    [refreshControl addTarget:self action:@selector(getChatHistory) forControlEvents:UIControlEventValueChanged];
    [self.table addSubview:refreshControl];
    
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.bottomView addGestureRecognizer:swipeDown];
}

- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Swipe Left");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Swipe Right");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
        NSLog(@"Swipe Up");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Swipe Down");
        [_messageField resignFirstResponder];
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidBeHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note {
    // Adjust the bottom constraint to make room for the keyboard.
    CGRect keyboardFrameEnd = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //self.bottomConstraint.constant = keyboardFrameEnd.size.height;
    //self.bottomConstraint.constant = 216;
    keyboard=keyboardFrameEnd.size.height;
    //   self.tableTopConstrait.constant = keyboard + 600;
    //self.topView.frame=CGRectMake(0, 20, self.view.frame.size.width, 50);
    //[self setFrame:keyboard];
    self.bottomViewConstraint.constant = keyboard;
    
//    CGSize Size=[Helper GetcgSizeFromNsstring:_messageField.text withFont:[UIFont fontWithName:@"AWConquerorDidot-Light" size:15] withCGSizeMake:CGSizeMake(_messageField.frame.size.width-100,MAXFLOAT)];
//    if (Size.height > 41 && Size.height < 200) {
//        self.bottomViewConstraint.constant = keyboard - 49 + Size.height + 10;
//        self.bottomViewConstraintHeight.constant = Size.height + 10;
//        _messageField.frame = CGRectMake(_messageField.frame.origin.x, _messageField.frame.origin.y, _messageField.frame.size.width, Size.height);
//        
//    }else if (Size.height <= 41){
//        self.bottomViewConstraint.constant = keyboard;
//        self.bottomViewConstraintHeight.constant = 41;
//    }
    //_messageField.selectedRange = NSMakeRange([_messageField.text length], 0);
    
    //self.bottomViewConstraintHeight.constant = 100;
    [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.2f];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    // Restores the bottom constraint to zero, making the view span the screen.
    //self.bottomConstraint.constant = 0;
    //  self.tableTopConstrait.constant = 0;
    keyboard = 0;
    self.bottomViewConstraint.constant = 49;
    self.bottomViewConstraintHeight.constant = 39;
    
//    CGSize Size=[Helper GetcgSizeFromNsstring:_messageField.text withFont:[UIFont fontWithName:@"AWConquerorDidot-Light" size:15] withCGSizeMake:CGSizeMake(_messageField.frame.size.width-100,MAXFLOAT)];
//    if (Size.height > 41 && Size.height < 200) {
//        _messageField.frame = CGRectMake(_messageField.frame.origin.x, _messageField.frame.origin.y, _messageField.frame.size.width, Size.height);
//        self.bottomViewConstraint.constant = keyboard - 49 + Size.height + 10;
//        self.bottomViewConstraintHeight.constant = Size.height + 10;
//    }else if (Size.height <= 41){
//        self.bottomViewConstraint.constant = 49;
//        self.bottomViewConstraintHeight.constant = 39;
//    }
    //_messageField.selectedRange = NSMakeRange([_messageField.text length], 0);
    
    [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.2f];
}

- (void)keyboardDidShow:(NSNotification *)note {
    [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.2f];
}

- (void)keyboardDidBeHidden:(NSNotification *)aNotification {
    [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.2f];
}

#pragma mark -- TableView Delegate

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float rowHeight=59.0f;
    NSArray *entry = [appDelegate.messages objectAtIndex:[indexPath row]];
    id<SINMessage> message = entry[0];
    CGSize Size=[Helper GetcgSizeFromNsstring:message.text withFont:[UIFont fontWithName:@"HelveticaNeue" size:15] withCGSizeMake:CGSizeMake(appDelegate.window.frame.size.width-125,MAXFLOAT)];
    //if (Size.height>rowHeight) {
        rowHeight=Size.height;
        rowHeight+=50;
   // }
    
    if ([message.text containsString:@"http"]) {
        rowHeight=170;
    }
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [appDelegate.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *entry = [appDelegate.messages objectAtIndex:[indexPath row]];
    
    id<SINMessage> message = entry[0];
    MessageTableViewCell *cell = [self dequeOrLoadMessageTableViewCell:[entry[1] intValue]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userProfileImage.layer.cornerRadius=cell.userProfileImage.frame.size.width/2;
    cell.userProfileImage.clipsToBounds=YES;
    cell.message.scrollEnabled=NO;
    cell.message.clipsToBounds=YES;
    //NSLog(@"reusable->%@", cell.reuseIdentifier);
    if ([entry[1] intValue] == 1) {
        [cell.userProfileImage setShowActivityIndicatorView:YES];
        [cell.userProfileImage setIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"image"])
        {
            [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 cell.userProfileImage.image = image;
             }];
        }else{
            cell.userProfileImage.image = [UIImage imageNamed:@"profile_rect.png"];
        }
        cell.message.layer.borderColor=[UIColor colorWithRed:230/255.0 green:231/255.0 blue:236/255.0 alpha:1].CGColor;
        cell.message.backgroundColor=[UIColor colorWithRed:230/255.0 green:231/255.0 blue:236/255.0 alpha:1];
        cell.message.textColor=[UIColor blackColor];
    }else{
        cell.message.layer.borderColor=[UIColor colorWithRed:25/255.0 green:47/255.0 blue:113/255.0 alpha:1].CGColor;
        cell.message.backgroundColor=[UIColor colorWithRed:44/255.0 green:96/255.0 blue:119/255.0 alpha:1];
        cell.message.textColor=[UIColor whiteColor];
        //cell.userProfileImage.image = [UIImage imageNamed:@"lt-small.png"];
        cell.userProfileImage.image = [UIImage imageNamed:@"profile.png"];
    }
    
    cell.userProfileImage.backgroundColor = [UIColor colorWithRed:230/255.0 green:231/255.0 blue:236/255.0 alpha:1];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.message.text = message.text;
    cell.message.layer.cornerRadius=5.0f;
    cell.message.layer.borderWidth=1.0f;
    cell.message.clipsToBounds=YES;
    cell.nameLabel.text = message.senderId;
    cell.timestampLabel.text = [_dateFormatter stringFromDate:message.timestamp];
    cell.message.font=_messageField.font;
    cell.message.layer.cornerRadius=10.0f;
    cell.message.clipsToBounds=YES;
    
    cell.sentImage.layer.cornerRadius=10.0f;
    cell.sentImage.clipsToBounds=YES;
    
    if ([message.text containsString:@"http"]) {
        cell.message.hidden=YES;
        cell.sentImage.hidden=NO;
        [cell.sentImage setShowActivityIndicatorView:YES];
        [cell.sentImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.sentImage sd_setImageWithURL:[NSURL URLWithString:message.text] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             cell.sentImage.image = image;
             [cell.sentImage setShowActivityIndicatorView:NO];
         }];
        [cell.buttonImageLarge addTarget:self action:@selector(openImagesScroller:) forControlEvents:UIControlEventTouchUpInside];
        cell.sentImage.tag=indexPath.row;
        cell.contentMode = UIViewContentModeScaleAspectFit;
        cell.buttonImageLarge.tag=indexPath.row;
    }else{
        cell.message.hidden=NO;
        cell.sentImage.hidden=YES;
    }
    
    if (indexPath.row==appDelegate.messages.count-1) {
    }
    
    return cell;
}

- (MessageTableViewCell *)dequeOrLoadMessageTableViewCell:(MessageDirection)direction {
    NSString *identifier =
    [NSString stringWithFormat:@"%@MessageCell", (Incoming == direction) ? @"Incoming" : @"Outgoing"];
    
    MessageTableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil][0];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)openImagesScroller:(UIButton*)sender{
    _slideImageViewController = [[PEARImageSlideViewController alloc] init];
    NSArray *entry = [appDelegate.messages objectAtIndex:sender.tag];
    id<SINMessage> message = entry[0];
    NSArray *imageArray=[[NSArray alloc] initWithObjects:message.text,nil];
    [_slideImageViewController setImageLists:imageArray];
    [_slideImageViewController showAtIndex:0];
}

#pragma mark -- Button Action

- (IBAction)sendMessage:(id)sender
{
    if (_messageField.text.length>0)
    {
        NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]];
        if ([receiverID isEqualToString:@"34"]) {
            receiverID=@"43";
        }else{
            receiverID=@"34";
        }
        
        [appDelegate sendMessage:_messageField.text receiverID:@"admin"];
        
        
        //////////////////////////
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:@"chatMessages" forKey:@"method"];
        [details setObject:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]] forKey:@"sender_id"];
        //[details setObject:receiverID forKey:@"reciever_id"];
        [details setObject:@"admin" forKey:@"reciever_id"];
        
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSString *timestamp=[dateFormat stringFromDate:[NSDate date]];
        
        [details setObject:timestamp forKey:@"sinch_msg_id"];
        [details setObject:_messageField.text forKey:@"message"];
        
        [api saveMessage:details];
        /////////////////////////////////////////////
        _messageField.text=@"";
        self.bottomViewConstraint.constant = keyboard;
        self.bottomViewConstraintHeight.constant = 41;
    }
}

-(void)sendImage:(NSString*)encodedImage{
    [Helper showIndicatorWithText:@"Uploading..." inView:self.view];
    NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]];
    if ([receiverID isEqualToString:@"34"]) {
        receiverID=@"43";
    }else{
        receiverID=@"34";
    }
    //[appDelegate sendMessage:_messageField.text receiverID:@"admin"];
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"chatMessages" forKey:@"method"];
    [details setObject:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]] forKey:@"sender_id"];
    //[details setObject:receiverID forKey:@"reciever_id"];
    [details setObject:@"admin" forKey:@"reciever_id"];
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString *timestamp=[dateFormat stringFromDate:[NSDate date]];
    
    [details setObject:timestamp forKey:@"sinch_msg_id"];
    [details setObject:encodedImage forKey:@"message_img"];
    
    //[Helper showIndicatorWithText:@"Loading..." inView:self.view];
    [api saveImageMessage:details];
    
    _messageField.text=@"";
    
}

- (IBAction)selectImage:(id)sender
{
    NSString *actionSheetTitle = @"LittleThings"; //Action Sheet Title
    NSString *camera = @"Camera";
    NSString *gallery = @"Gallery";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:camera, gallery, nil];
    actionSheet.tag=102;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [myAlertView show];
        }else{
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.navigationBar.tintColor = [UIColor blackColor];
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }else if(buttonIndex == 1){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.navigationBar.tintColor = [UIColor blackColor];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

#pragma mark - image delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.cameraImage = chosenImage;
    //[self.continueBtn setBackgroundImage:IMAGE(@"buttonGolden") forState:UIControlStateNormal];
    //compress image
    CGSize newSize = CGSizeMake(450.0f, 450.0f);
    UIGraphicsBeginImageContext(newSize);
    [chosenImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //NSString *encodedImage=[self imageToNSString:newImage];
    NSString *encodedImage=[self imageToNSString:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self performSelector:@selector(sendImage:) withObject:encodedImage afterDelay:0.1f];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        //[self.message becomeFirstResponder];
    }];
}

#pragma mark - base 64
- (NSString *)base64String:(UIImage*)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(NSString *)imageToNSString:(UIImage *)image{
    NSData *imageData = UIImagePNGRepresentation(image);
    //return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [imageData base64EncodedStringWithOptions:NSUTF8StringEncoding];
}

-(UIImage *)stringToUIImage:(NSString *)string{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}


#pragma mark -- Notification Method

-(void)openCalendar
{
    self.tabBarController.selectedIndex=1;
}

-(void)hideKeyboard
{
    [_messageField resignFirstResponder];
}

#pragma mark -- AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BuyCreditsViewController"];
            controller.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];

    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -- TextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write something here..."]){
        textView.text = @"";
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [UIView commitAnimations];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]){
        textView.text = @"Write something here...";
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView commitAnimations];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    CGSize Size=[Helper GetcgSizeFromNsstring:textView.text withFont:[UIFont fontWithName:@"HelveticaNeue" size:15] withCGSizeMake:CGSizeMake(textView.frame.size.width-100,MAXFLOAT)];

    if (Size.height > 41 && Size.height < 200) {
        textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, Size.height + 10);
        self.bottomViewConstraint.constant = keyboard - 49 + Size.height + 10;
        self.bottomViewConstraintHeight.constant = Size.height + 10;
    }else if (Size.height >= 200) {
        textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, 200 + 10);
        self.bottomViewConstraint.constant = keyboard - 49 + 200 + 10;
        self.bottomViewConstraintHeight.constant = 200;
    }else if (Size.height <= 41){
        self.bottomViewConstraint.constant = keyboard;
        self.bottomViewConstraintHeight.constant = 41;
    }
    
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView{
     [textView scrollRangeToVisible:textView.selectedRange];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- Dashboard incoming message delegate
- (void)dashboardMessageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId{
    [Helper hideIndicatorFromView:self.view];
    [_messages addObject:@[ message, @(Outgoing) ]];
    [self.table reloadData];
    [self scrollToBottom];
}
- (void)dashboardMessageClient:(id<SINMessageClient>)messageClient didReceiveIncomingMessage:(id<SINMessage>)message{
    NSLog(@"message dashboard received");
    [_messages addObject:@[ message, @(Incoming) ]];
    [self.table reloadData];
    [self scrollToBottom];
}

/*
 Scrolls the message view to the bottom to ensure we always see the latest message.
 */
- (void)scrollToBottom {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(appDelegate.messages.count - 1)inSection:0];
    if (appDelegate.messages.count) {
        [self.table scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

-(void)getChatHistory{
    if (pageNo==totalPage) {
        [refreshControl endRefreshing];
        return;
    }else if (pageNo < totalPage){
        pageNo++;
    }
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setObject:@"showchatMessages" forKey:@"method"];
    
    NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]];
    [details setObject:[NSString stringWithFormat:@"%@", receiverID] forKey:@"uid"];
    [details setObject:[NSString stringWithFormat:@"%d", pageNo] forKey:@"page"];
    if (!refreshControl.isRefreshing) {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
    }
    [api getChatHistory:details];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response{
    [Helper hideIndicatorFromView:self.view];
    if (refreshControl.isRefreshing) {
        [refreshControl endRefreshing];
    }
    if (response != nil) {
    }else{
    }
}

-(void)callbackFromSaveImageMessage:(id)response{
    if (refreshControl.isRefreshing) {
        [refreshControl endRefreshing];
    }
    if (response != nil) {
        if ([response valueForKey:@"textMessage"]) {
            NSString *textMsg=[NSString stringWithFormat:@"%@",[response valueForKey:@"textMessage"]];
            if (textMsg.length>0)
            {
                NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]];
                if ([receiverID isEqualToString:@"34"]) {
                    receiverID=@"43";
                }else{
                    receiverID=@"34";
                }
                
                [appDelegate sendMessage:textMsg receiverID:@"admin"];
                
            }
        }
    }else{
        
    }
}

-(void)callbackGetChatMessages:(id)response
{
    [Helper hideIndicatorFromView:self.view];
    if (refreshControl.isRefreshing) {
        [refreshControl endRefreshing];
    }
    if (response != nil) {
        NSLog(@"%@",response);
        if ([response valueForKey:@"total_pages"]) {
            totalPage=[[NSString stringWithFormat:@"%@",[response valueForKey:@"total_pages"]] intValue];
        }
        NSMutableArray *userObjArray=[[NSMutableArray alloc]init];
        NSArray *userArray=(NSArray*)[response valueForKey:@"chat_users"];
        for (NSDictionary *userDic in userArray) {
            NSString *receiverID=@"";
            NSString *senderID=@"";
            NSString *text=@"";
            NSString *dateTimeStr=@"";
            BOOL incoming = NO;
            if ([userDic valueForKey:@"receiverid"]) {
                receiverID=[NSString stringWithFormat:@"%@",[userDic valueForKey:@"receiverid"]];
            }
            if ([userDic valueForKey:@"senderid"]) {
                senderID=[NSString stringWithFormat:@"%@",[userDic valueForKey:@"senderid"]];
            }
            if ([userDic valueForKey:@"textMessage"]) {
                text=[NSString stringWithFormat:@"%@",[userDic valueForKey:@"textMessage"]];
            }
            
            if ([userDic valueForKey:@"createdon"]) {
                dateTimeStr=[NSString stringWithFormat:@"%@",[userDic valueForKey:@"createdon"]];
            }
            
            Message *message = [[Message alloc]init];
            if ([receiverID isEqualToString:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]]]) {
                message.senderId=receiverID;
                message.recipientIds=[[NSArray alloc]initWithObjects:senderID, nil];
                incoming=YES;
            }else{
                message.recipientIds=[[NSArray alloc]initWithObjects:receiverID, nil];
                message.senderId=senderID;
                incoming=NO;
            }
            message.text=text;
            
            NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            message.timestamp=[dateFormat dateFromString:dateTimeStr];
            
            if (message.timestamp == nil) {
                if ([userDic valueForKey:@"modified"]) {
                    dateTimeStr=[NSString stringWithFormat:@"%@",[userDic valueForKey:@"modified"]];
                    message.timestamp=[dateFormat dateFromString:dateTimeStr];
                }
            }
            
            
            if (incoming) {
                [userObjArray addObject:@[ message, @(0) ]];
            }else{
                [userObjArray addObject:@[ message, @(1) ]];
            }
        }
        if (appDelegate.messages.count) {
            if (pageNo==1) {
                appDelegate.messages=userObjArray;
            }else{
                [userObjArray addObjectsFromArray:appDelegate.messages];
                appDelegate.messages=userObjArray;
            }
        }else{
            appDelegate.messages=userObjArray;
        }
        [refreshControl endRefreshing];
        [self.table reloadData];
        if (pageNo==1) {
            [self scrollToBottom];
        }
    }
    
}

@end
