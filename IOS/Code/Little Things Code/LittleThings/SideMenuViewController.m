//
//  SideMenuViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "SideMenuViewController.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

-(void)viewWillAppear:(BOOL)animated
{
    _name.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    _email.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"])
    {
        NSData* data = [[NSData alloc] initWithBase64EncodedString:[[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage* image = [UIImage imageWithData:data];
        _userImage.image = image;
        _coverImage.image = image;
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"image"])
    {
        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
        {
            _userImage.image = image;
            _coverImage.image = image;
        }];
    }
    
    [_table reloadData];
    
    [api getMyCredits];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _userImage.layer.cornerRadius = 100/2;
    _userImage.layer.borderColor = [UIColor whiteColor].CGColor;
    _userImage.layer.borderWidth = 2;
    _userImage.clipsToBounds=YES;
    
    list=[[NSArray alloc]initWithObjects:@"Home", @"My Profile", @"Buy Credits", @"Payment Details", @"My Orders", @"Calendar", @"FAQ", @"Support", @"Visit Us", @"Sign Out", nil];
    iconList=[[NSArray alloc]initWithObjects:@"home_side.png", @"user_profile.png", @"credit_side.png", @"dollar.png", @"order_side.png", @"calender_side.png", @"faq_side.png", @"support_side.png", @"visit_side.png", @"logout_side.png", nil];
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
}

#pragma mark -- UItableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DrawerCell *cell;//
    cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row]];
    
    for (UIView *subView in cell.contentView.subviews)
    {
        [subView removeFromSuperview];
    }
    
    if (cell == nil)
    {
        cell = [[DrawerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[list objectAtIndex:indexPath.row]];
    }
    
    cell.icon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 9, 30, 30)];
    cell.icon.image = [UIImage imageNamed:[iconList objectAtIndex:indexPath.row]];
    cell.icon.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.title=[[UILabel alloc]initWithFrame:CGRectMake(59, 14, 200, 21)];
    cell.title.text=[list objectAtIndex:indexPath.row];
    cell.title.font=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:18];
    cell.title.textColor = [UIColor whiteColor];
    
    cell.credits=[[UILabel alloc]initWithFrame:CGRectMake(168, 9, 84, 30)];
    cell.credits.font=[UIFont fontWithName:@"MrEavesSanOT-Reg" size:16];
    cell.credits.textColor = [UIColor whiteColor];
    cell.credits.textAlignment = NSTextAlignmentRight;
    
    if (indexPath.row==2)
    {
        cell.credits.hidden=NO;
        cell.credits.text=[NSString stringWithFormat:@"%@ Credits", [[NSUserDefaults standardUserDefaults] objectForKey:@"credit"]];
    }
    else
    {
        cell.credits.hidden=YES;
    }
    
    UIImageView *seprator=[[UIImageView alloc]initWithFrame:CGRectMake(0, 48, self.view.frame.size.width, 1)];
    seprator.backgroundColor=[Helper colorFromHexString:@"3B434C"];
    
    [cell.contentView addSubview:cell.icon];
    [cell.contentView addSubview:cell.title];
    [cell.contentView addSubview:cell.credits];
    [cell.contentView addSubview:seprator];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==5)
    {
        //[self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCalendar" object:nil];
    }
    else if (indexPath.row==7)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            NSArray *recipients = [NSArray arrayWithObject:@"hello@thelittlethings.company"];
            [picker setToRecipients:recipients];
            [picker setSubject:@""];
            [picker setMessageBody:@"" isHTML:NO];
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:@"No Email" message:@"No mail account setup on your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
    else if (indexPath.row==8)
    {
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.thelittlethings.company/"]];
    }
    else if (indexPath.row==9)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign Out" message:@"Do you really want to sign out?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.tag=1;
        [alert show];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark -- SWRevealViewController Seague

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
    {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
        {
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController addChildViewController:dvc];
            [navController setViewControllers:navController.childViewControllers];
        };
    }
}

#pragma mark -- AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            [Helper removeAllKeysFromUserDefaults];
            
            UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            controller.hidesBottomBarWhenPushed=YES;
            
            AppDelegate *delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
            delegate.client = nil;
            [delegate.messages removeAllObjects];
            UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
            [navController setViewControllers:@[controller] animated:YES];
            
        }
    }
}

#pragma mark -- MFMailComposure Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    
}

-(void)callBackMyCreditsSuccess:(id)response
{
    [_table reloadData];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
