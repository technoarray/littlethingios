//
//  TermsViewController.h
//  LittleThings
//
//  Created by Saurav on 25/05/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *text;
- (IBAction)back:(id)sender;
@end
