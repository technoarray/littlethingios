//
//  SinchVideoHelper.h
//  SinchVideoCallDemo
//
//  Created by Saurav on 06/01/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Sinch/Sinch.h>
#import "AppDelegate.h"

@interface SinchVideoHelper : NSObject<SINClientDelegate>
{
  
}

@property (strong, nonatomic) id<SINClient> client;

+ (instancetype)shareInstance;
- (void)initSinchClientWithUserId:(NSString *)userId;
- (id<SINCall>)callSinchClientWithUserDetails:(NSDictionary *)userDetails;
- (NSString *)pathForSound:(NSString *)soundName;
@end
