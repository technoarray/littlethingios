//
//  OrdersViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "SWRevealViewController.h"
#import "PaymentDetailsViewController.h"

@interface OrdersViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, APIsDelegate>
{
    NSMutableArray *orderList;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UITableView *table;
- (IBAction)back:(id)sender;
- (IBAction)search:(id)sender;
@end
