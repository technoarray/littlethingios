//
//  ForgotPasswordViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate, APIsDelegate>
{
    UITextField *activeField;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UILabel *loginLbl;
- (IBAction)submit:(id)sender;
@end
