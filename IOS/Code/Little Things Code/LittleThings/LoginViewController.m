//
//  LoginViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loginBtn.layer.cornerRadius = 45/2;
    _loginBtn.clipsToBounds=YES;
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_signUpLbl.text];
    //Don't Have An Account?Signup Now
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:212/255.0 green:151/255.0 blue:151/255.0 alpha:1] range:NSMakeRange(26, 10)];
    [_signUpLbl setAttributedText: text];
    
    _signUpLbl.userInteractionEnabled=YES;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(signUp)];
    gesture.numberOfTapsRequired=1;
    [_signUpLbl addGestureRecognizer:gesture];
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    //_email.text=@"iosdeveloper@gmail.com";
    //_password.text=@"123";
}

#pragma mark -- button Action

- (IBAction)login:(id)sender
{
    if (_email.text.length == 0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Email cannot be empty"];
    }
    else if ([Helper validateEmailWithString:_email.text]==NO)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter valid email address."];
    }
    else if (_password.text.length==NO)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Password cannot be empty"];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:_email.text forKey:@"email"];
        [details setObject:_password.text forKey:@"password"];
        [details setObject:@"1" forKey:@"deviceType"];
        [details setObject:[Helper getNotificationToken] forKey:@"deviceToken"];
        
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api loginUserWithDetails:details];
    }
}

- (IBAction)Forgot:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)signUp
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackLoginSuccess:(id)response
{
    //[Helper registerForSinch];
    NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"email"]];
    //    if ([receiverID isEqualToString:@"34"]) {
    //        receiverID=@"admin";
    //    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate initSinchClientWithUserId:receiverID];
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.view.frame.size.width==375)
    {
        activeField=textField;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-120, self.view.frame.size.width, self.view.frame.size.height)];
        
        [UIView commitAnimations];
    }
    else if (self.view.frame.size.height==568)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-180, self.view.frame.size.width, self.view.frame.size.height)];
        [UIView commitAnimations];
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField==activeField)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
    else
    {
        if (self.view.frame.size.width==375)
        {
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+120, self.view.frame.size.width, self.view.frame.size.height)];
        }
        else if (self.view.frame.size.height==568)
        {
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+180, self.view.frame.size.width, self.view.frame.size.height)];
        }
    }
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_password becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
