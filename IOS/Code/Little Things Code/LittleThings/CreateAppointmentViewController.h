//
//  CreateAppointmentViewController.h
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"

@interface CreateAppointmentViewController : UIViewController<UITextFieldDelegate, APIsDelegate, UITextViewDelegate>
{
    WebAPI *api;
    NSDate *startDate, *endDate;
    UIToolbar *keyboardToolbar;
    UITextField *activeField;
}

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDictionary *selectedAppointment;

@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UITextField *timeFromTxt;
@property (strong, nonatomic) IBOutlet UITextField *timeToTxt;
@property (strong, nonatomic) IBOutlet UITextField *appointmentWith;
@property (strong, nonatomic) IBOutlet UITextField *location;
@property (strong, nonatomic) IBOutlet UITextView *appointmentDesc;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;

- (IBAction)back:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;
@end
