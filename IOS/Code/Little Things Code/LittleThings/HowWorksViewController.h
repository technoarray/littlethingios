//
//  HowWorksViewController.h
//  LittleThings
//
//  Created by Saurav on 31/05/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "WebAPI.h"
#import "PaymentDetailsViewController.h"

@interface HowWorksViewController : UIViewController<UITextFieldDelegate, APIsDelegate>
{
    WebAPI *api;
}

@property (strong, nonatomic) NSDictionary *creditInfo;
@property BOOL isBuyingCredits;
@property BOOL isPayingForInvoice;
@property BOOL showBackBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *questionHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *exampleHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *firstHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *thirdHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *question;
@property (strong, nonatomic) IBOutlet UILabel *example;
@property (strong, nonatomic) IBOutlet UILabel *secondWidth;
@property (strong, nonatomic) IBOutlet UILabel *firstPoint;
@property (strong, nonatomic) IBOutlet UITextField *couponTxt;
@property (strong, nonatomic) IBOutlet UILabel *thirdPoint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;

- (IBAction)buyCredits:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)applyCode:(id)sender;
@end
