//
//  DiscoverViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import <UIImageView+WebCache.h>

@interface DiscoverViewController : UIViewController<APIsDelegate>
{
    NSArray *list;
    WebAPI *api;
}

@property (strong, nonatomic) NSDictionary *selectedSubcategory;

@property (strong, nonatomic) IBOutlet UIButton *sideMenu;
@property (strong, nonatomic) IBOutlet UITextView *discoverMessage;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UILabel *name;
@end
