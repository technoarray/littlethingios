//
//  UpdateProfileViewController.m
//  LittleThings
//
//  Created by Saurav on 29/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "UpdateProfileViewController.h"

@interface UpdateProfileViewController ()

@end

@implementation UpdateProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _scroll.scrollEnabled=YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"])
    {
        NSData* data = [[NSData alloc] initWithBase64EncodedString:[[NSUserDefaults standardUserDefaults] objectForKey:@"imageString"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage* image = [UIImage imageWithData:data];
        _userImage.image = image;
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"image"])
    {
        [_userImage sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             _userImage.image = image;
         }];
    }
    
    _userImage.layer.cornerRadius=86/2;
    _userImage.layer.borderColor=[UIColor whiteColor].CGColor;
    _userImage.layer.borderWidth=3;
    _userImage.clipsToBounds=YES;
    
    _imageBtn.layer.cornerRadius=15;
    _imageBtn.clipsToBounds=YES;
    
    _saveBtn.layer.cornerRadius=20;
    _saveBtn.clipsToBounds=YES;
    
    _cancelBtn.layer.cornerRadius=20;
    _cancelBtn.clipsToBounds=YES;
    _cancelBtn.layer.borderColor=_saveBtn.backgroundColor.CGColor;
    _cancelBtn.layer.borderWidth=2;
    
    _name.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    _email.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    _address.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
    _gender.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"];
    _city.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"city"];
    _phone.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"phone_number"];
    
    _email.enabled=NO;
    
    UIImageView *dropImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage.contentMode = UIViewContentModeCenter;
    dropImage.image = [UIImage imageNamed:@"down-arrow.png"];
    _gender.rightView = dropImage;
    _gender.rightViewMode = UITextFieldViewModeAlways;
    
    MMNumberKeyboard *keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _phone.inputView = keyboard;
    
    keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _zip.inputView = keyboard;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"delete" ofType:@"png"];
    //NSData *data = [NSData dataWithContentsOfFile:path];
    //imageString = [NSString stringWithFormat:@"data:image/jpeg;base64,%@", [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    
    _imageBtnXCoordinate.constant = self.view.frame.size.width/2 + 20;
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender
{
    if (_name.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Name cannot be empty."];
    }
    else if (_phone.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Phone number cannot be empty."];
    }
    else if (_phone.text.length!=10)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter 10 didgit phone number."];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:_name.text forKey:@"name"];
        [details setObject:_gender.text forKey:@"gender"];
        [details setObject:_phone.text forKey:@"phone_number"];
        [details setObject:_city.text forKey:@"city"];
        [details setObject:_address.text forKey:@"address"];
        [details setObject:_state.text forKey:@"state"];
        [details setObject:_zip.text forKey:@"zipcode"];
        [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
        if (imageString)
        {
            [details setObject:imageString forKey:@"profile_image"];
        }
        
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api updateProfileWithDetails:details];
    }
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeImage:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Capture Image", @"Photo Library", nil];
    sheet.tag=2;
    [sheet showInView:self.view];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackUpdateProfileSuccess:(id)response
{
    if (imageString)
    {
        [[NSUserDefaults standardUserDefaults] setObject:imageString forKey:@"imageString"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [Helper showAlertViewWithTitle:@"Profile Updated" message:@"Profile updated successfully"];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- ImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    _userImage.image=image;
    _userImage.contentMode=UIViewContentModeScaleAspectFill;
    _userImage.layer.cornerRadius=86/2;
    _userImage.clipsToBounds=YES;
    NSData *imageData = UIImageJPEGRepresentation(image, 0.2);
    imageString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark -- Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==1)
    {
        if (buttonIndex==0)
        {
            _gender.text = @"Male";
        }
        else if(buttonIndex == 1)
        {
            _gender.text = @"Female";
        }
    }
    else if (actionSheet.tag==2)
    {
        if (buttonIndex == 0)
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                controller.sourceType = UIImagePickerControllerSourceTypeCamera;
                controller.delegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"No Camera!" message:@"Camera not available!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            }
        }
        else if (buttonIndex == 1)
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==3)
    {
        [self.view endEditing:YES];
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"Gender" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Male", @"Female", nil];
        sheet.tag=1;
        [sheet showInView:self.view];
        return NO;
    }
    else
    {
        activeField=textField;
        if (self.view.frame.size.width==375)
        {
            if (textField.tag == 5 || textField.tag == 6 || textField.tag == 7 || textField.tag == 8)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-210, self.view.frame.size.width, self.view.frame.size.height)];
                [UIView commitAnimations];
            }
        }
        else if (self.view.frame.size.height==568)
        {
            if (textField.tag == 4 || textField.tag == 5 || textField.tag == 6)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-200, self.view.frame.size.width, self.view.frame.size.height)];
                [UIView commitAnimations];
            }
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField==activeField)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
    else
    {
        if (self.view.frame.size.width==375)
        {
            if (textField.tag == 5 || textField.tag == 6 || textField.tag == 7 || textField.tag == 8)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+210, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
        else if (self.view.frame.size.height==568)
        {
            if (textField.tag == 4 || textField.tag == 5 || textField.tag == 6)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+200, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    }
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==4)
    {
        [_city becomeFirstResponder];
    }
    else if (textField.tag==5)
    {
        [_state becomeFirstResponder];
    }
    else if (textField.tag==5)
    {
        [_state becomeFirstResponder];
    }
    else if (textField.tag==7)
    {
        [_zip becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
