//
//  AppointmentsViewController.m
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "AppointmentsViewController.h"

@interface AppointmentsViewController ()

@end

@implementation AppointmentsViewController

-(void)viewWillAppear:(BOOL)animated
{
    if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        if (appointmentList.count==0)
        {
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        }
        [api getAllAppointmentsForDate:_selectedDate];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _appointmentBtn.layer.cornerRadius = 50/2;
    _appointmentBtn.clipsToBounds = YES;
    
    _appointmentLbl.text = [Helper convertDateForToLbl:_selectedDate];
    
    api = [[WebAPI alloc]init];
    api.delegate = self;
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackAppointmentListSuccess:(id)response
{
    appointmentList = [response objectForKey:@"user_events"];
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appointmentList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"appointmentCell"];
    
    UILabel *date = [cell viewWithTag:1];
    UILabel *time = [cell viewWithTag:2];
    UILabel *name = [cell viewWithTag:3];
    UILabel *info = [cell viewWithTag:4];
    
    NSDictionary *dict = [appointmentList objectAtIndex:indexPath.row];
    
    date.text = [dict objectForKey:@"time_from"];
    time.text = [dict objectForKey:@"time_diff"];
    name.text = [dict objectForKey:@"appointment_with"];
    if ([[dict objectForKey:@"appointment_descp"] isEqualToString:@"Notes"])
    {
        info.text = @"";
    }
    else
    {
        info.text = [dict objectForKey:@"appointment_descp"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppointmentDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AppointmentDetailViewController"];
    controller.selectedDate=_selectedDate;
    controller.details = [appointmentList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -- Button Action

- (IBAction)addAppointment:(id)sender
{
    CreateAppointmentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateAppointmentViewController"];
    controller.selectedDate=_selectedDate;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
