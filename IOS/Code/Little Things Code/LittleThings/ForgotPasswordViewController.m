//
//  ForgotPasswordViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _submitBtn.layer.cornerRadius = 45/2;
    _submitBtn.clipsToBounds=YES;
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_loginLbl.text];
    //Go back to Login Page.Click here
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:212/255.0 green:151/255.0 blue:151/255.0 alpha:1] range:NSMakeRange(27, 10)];
    [_loginLbl setAttributedText: text];
    
    _loginLbl.userInteractionEnabled=YES;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(login)];
    gesture.numberOfTapsRequired=1;
    [_loginLbl addGestureRecognizer:gesture];
    
    api = [[WebAPI alloc]init];
    api.delegate = self;
}

#pragma mark -- Button Action

- (IBAction)submit:(id)sender
{
    if (_email.text.length == 0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Email cannot be empty"];
    }
    else if ([Helper validateEmailWithString:_email.text]==NO)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter valid email address."];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:_email.text forKey:@"email"];
        
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api forgotPasswordWithDetails:details];
    }
}

-(void)login
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackForgotPasswordSuccess:(id)response
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (self.view.frame.size.width==375)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
    }
    else if (self.view.frame.size.height==568)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-180, self.view.frame.size.width, self.view.frame.size.height)];
    }
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
