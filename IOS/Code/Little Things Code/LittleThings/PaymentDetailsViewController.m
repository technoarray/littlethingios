//
//  PaymentDetailsViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "PaymentDetailsViewController.h"

@interface PaymentDetailsViewController ()

@end

@implementation PaymentDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_showBackBtn==YES)
    {
        [_menuBtn setImage:[UIImage imageNamed:@"right-arrow.png"] forState:UIControlStateNormal];
        [_menuBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    
    _monthWidthConstraint.constant = (self.view.frame.size.width)/2 - 53;
    _imageXCoordinateConstraint.constant = 8 + (self.view.frame.size.width)/2 - 53;
    
    UIImageView *dropImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage.contentMode = UIViewContentModeCenter;
    dropImage.image = [UIImage imageNamed:@"down-arrow.png"];
    _expiryYear.rightView = dropImage;
    _expiryYear.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *dropImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage1.contentMode = UIViewContentModeCenter;
    dropImage1.image = [UIImage imageNamed:@"down-arrow.png"];
    _expiryMonth.rightView = dropImage1;
    _expiryMonth.rightViewMode = UITextFieldViewModeAlways;
    
    MMNumberKeyboard *keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _cardNumber.inputView = keyboard;
    
    keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _securityCode.inputView = keyboard;
    
    monthList = [NSArray arrayWithObjects:@"January (01)", @"Febuary (02)", @"March (03)", @"April (04)", @"May (05)", @"June (06)", @"July (07)", @"August (08)", @"September (09)", @"October (10)", @"November (11)", @"December (12)", nil];
    yearList = [NSArray arrayWithObjects:@"2017", @"2018", @"2019", @"2020", @"2021", @"2022", @"2023", @"2024", @"2025", nil];
    
    api = [[WebAPI alloc]init];
    api.delegate = self;
    
    _savedCardView.hidden=YES;
    
    _firstDot.layer.cornerRadius = 6;
    _firstDot.clipsToBounds = YES;
    
    _secondDot.layer.cornerRadius = 6;
    _secondDot.clipsToBounds = YES;
    
    _thirdDot.layer.cornerRadius = 6;
    _thirdDot.clipsToBounds = YES;
    
    _fourthDot.layer.cornerRadius = 6;
    _fourthDot.clipsToBounds = YES;
    
    _fifthDot.layer.cornerRadius = 6;
    _fifthDot.clipsToBounds = YES;
    
    _sixthDot.layer.cornerRadius = 6;
    _sixthDot.clipsToBounds = YES;
    
    _seventhDot.layer.cornerRadius = 6;
    _seventhDot.clipsToBounds = YES;
    
    _eigthDot.layer.cornerRadius = 6;
    _eigthDot.clipsToBounds = YES;
    
    _ninthDot.layer.cornerRadius = 6;
    _ninthDot.clipsToBounds = YES;
    
    _tenthDot.layer.cornerRadius = 6;
    _tenthDot.clipsToBounds = YES;
    
    _eleventhDot.layer.cornerRadius = 6;
    _eleventhDot.clipsToBounds = YES;
    
    _twelthDot.layer.cornerRadius = 6;
    _twelthDot.clipsToBounds = YES;
    
    _scroll.layer.borderColor=_firstDot.backgroundColor.CGColor;
    _scroll.layer.borderWidth=0.5;
    
    [Helper showIndicatorWithText:@"Loading..." inView:self.view];
    [api getStripeUserDetails:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"]];
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submit:(id)sender
{
    if (_cardNumber.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter valid card number."];
    }
    else if (_expiryMonth.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please select expiry month."];
    }
    else if (_expiryYear.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please select expiry year."];
    }
    else if (_securityCode.text.length!=3)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter valid CVV code."];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        STPCardParams *cardParams = [[STPCardParams alloc] init];
        cardParams.number = _cardNumber.text;
        cardParams.expMonth = [_expiryMonth.text integerValue];
        cardParams.expYear = [_expiryYear.text integerValue];
        cardParams.cvc = _securityCode.text;
        
        [self generateTokenForCard:cardParams];
    }
}

- (IBAction)applyCoupon:(id)sender
{
    if (_couponTxt.text.length>0)
    {
        if ([Helper isInternetConnected]==YES)
        {
            [Helper showIndicatorWithText:@"Applying..." inView:self.view];
            [api applyCouponCode:_couponTxt.text];
        }
        else
        {
            [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
        }
    }
}

-(void)done:(id)button
{
    [_expiryMonth resignFirstResponder];
    [_expiryYear resignFirstResponder];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)buyCredits:(UITapGestureRecognizer *)gesture
{
    if (_isPayingForInvoice==YES)
    {
        UIView *view = gesture.view;
        selectedIndex = view.tag;
        NSDictionary *dict = [cardList objectAtIndex:view.tag];
        NSLog(@"%@", dict);
        
        [self.view endEditing:YES];
        
        NSString *title = [NSString stringWithFormat:@"%@ Card", [dict objectForKey:@"brand"]];
        NSString *message = [NSString stringWithFormat:@"Do you really want to charge this card for $%@.", [_selectedCredit objectForKey:@"amount"]];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Buy", nil];
        alert.tag=2;
        [alert show];
    }
    else if (_isBuyingCredits==YES)
    {
        UIView *view = gesture.view;
        selectedIndex = view.tag;
        NSDictionary *dict = [cardList objectAtIndex:view.tag];
        NSLog(@"%@", dict);
        
        [self.view endEditing:YES];
        
        NSString *title = [NSString stringWithFormat:@"%@ Card", [dict objectForKey:@"brand"]];
        NSString *message;
        if (_discountDetails)
        {
            message = [NSString stringWithFormat:@"Do you really want to buy %@ for $%.2f.", [_selectedCredit objectForKey:@"credits"], ([[_selectedCredit objectForKey:@"price"] floatValue]*(100-[[_discountDetails objectForKey:@"discount"] floatValue]))/100];
        }
        else
        {
            message = [NSString stringWithFormat:@"Do you really want to buy %@ for $%@.", [_selectedCredit objectForKey:@"credits"], [_selectedCredit objectForKey:@"price"]];
        }
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Buy", nil];
        alert.tag=1;
        [alert show];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackStripeUserDetailsSuccess:(id)response
{
    NSDictionary *dict = [response objectForKey:@"sources"];
    if ([dict objectForKey:@"data"])
    {
        cardList = [dict objectForKey:@"data"];
        if (cardList.count>0)
        {
            _savedCardView.hidden=NO;
            [self showCards];
        }
    }
}

-(void)callBackSaveCardSuccess:(id)response
{
    _cardNumber.text=@"";
    _expiryMonth.text=@"";
    _expiryYear.text=@"";
    _securityCode.text=@"";
    
    if ([_selectedCredit objectForKey:@"id"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InvoicePaid" object:_selectedCredit];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [api getStripeUserDetails:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"]];
    }
}

-(void)callBackBuyCreditsSuccess:(STPCardParams *)response
{
    _isPayingForInvoice=NO;
    _isBuyingCredits=NO;
    _cardNumber.text=@"";
    _expiryMonth.text=@"";
    _expiryYear.text=@"";
    _securityCode.text=@"";
    
    if (response)
    {
        [self generateTokenForCard:response];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InvoicePaid" object:_selectedCredit];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)callBackApplyCouponSuccess:(id)response
{
    _couponTxt.text=@"";
    _discountDetails = response;
}

#pragma mark -- Helper Methods

-(void)showMonthPickerView
{
    UIPickerView *picker = [[UIPickerView alloc]init];
    picker.dataSource = self;
    picker.delegate = self;
    picker.tag=1;
    picker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    doneButton.tag=1;
    [doneButton setTintColor:[UIColor whiteColor]];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-
                                     picker.frame.size.height-50, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    _expiryMonth.inputView = picker;
    _expiryMonth.inputAccessoryView = toolBar;
    
    if (_expiryMonth.text.length==0)
    {
        [self showMonthForSelectedIndex:0];
    }
}

-(void)showYearPickerView
{
    UIPickerView *picker = [[UIPickerView alloc]init];
    picker.dataSource = self;
    picker.delegate = self;
    picker.tag=2;
    picker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(done:)];
    doneButton.tag=2;
    [doneButton setTintColor:[UIColor whiteColor]];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:
                          CGRectMake(0, self.view.frame.size.height-
                                     picker.frame.size.height-50, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             doneButton, nil];
    [toolBar setItems:toolbarItems];
    _expiryYear.inputView = picker;
    _expiryYear.inputAccessoryView = toolBar;
    
    if (_expiryYear.text.length==0)
    {
        [self showYearForSelectedIndex:0];
    }
}

-(void)showMonthForSelectedIndex:(long)index
{
    NSString *month=[monthList objectAtIndex:index];
    NSArray *list = [month componentsSeparatedByString:@" "];
    month = [list objectAtIndex:1];
    month = [month substringWithRange:NSMakeRange(1, 2)];
    _expiryMonth.text = month;
}

-(void)showYearForSelectedIndex:(long)index
{
    NSString *year=[yearList objectAtIndex:index];
    year = [year substringWithRange:NSMakeRange(2, 2)];
    _expiryYear.text = year;
}

-(void)showCards
{
    float xcoordinate = 0;
    int index = 0;
    for (NSDictionary *dict in cardList)
    {
        UIView *view  = [[UIView alloc]initWithFrame:CGRectMake(xcoordinate, 0, _scroll.frame.size.width, _scroll.frame.size.height)];
        view.backgroundColor = [UIColor clearColor];
        view.tag = index;
        view.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buyCredits:)];
        gesture.numberOfTapsRequired=1;
        [view addGestureRecognizer:gesture];
        
        UILabel *cardType = [[UILabel alloc]initWithFrame:CGRectMake(17, 16, 231, 24)];
        cardType.font = [UIFont boldSystemFontOfSize:18];
        cardType.textColor = [UIColor whiteColor];
        cardType.text = [NSString stringWithFormat:@"%@ Card", [dict objectForKey:@"brand"]];
        //[view addSubview:cardType];
        
        UIImageView *cardImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 16, 80, 24)];
        cardImage.contentMode = UIViewContentModeCenter;
        if ([[dict objectForKey:@"brand"] isEqualToString:@"Visa"])
        {
            cardImage.image = [PaymentDetailsViewController loadImageFromResourceBundle:@"stp_card_visa.png"];
        }
        
        [view addSubview:cardImage];
        
        UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(236, 59, 40, 20)];
        number.font = [UIFont systemFontOfSize:16];
        number.textColor = _firstDot.backgroundColor;
        number.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"last4"]];
        [view addSubview:number];
        
        [_scroll addSubview:view];
        
        xcoordinate = xcoordinate + view.frame.size.width;
        index = index + 1;
    }
    
    _scroll.contentSize = CGSizeMake(xcoordinate, _scroll.frame.size.height);
}

-(void)generateTokenForCard:(STPCardParams *)cardParams
{
    [Helper showIndicatorWithText:@"Loading..." inView:self.view];
    
    STPCardValidationState state = [STPCardValidator validationStateForCard:cardParams];
    if (state == STPCardValidationStateValid)
    {
        [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
            if (error) {
                // show the error, maybe by presenting an alert to the user
                NSLog(@"%@", error);
                [Helper hideIndicatorFromView:self.view];
                [Helper showAlertViewWithTitle:@"Error" message:[error localizedDescription]];
            } else
            {
                NSLog(@"%@", token);
                if (_isPayingForInvoice==YES)
                {
                    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
                    [details setObject:@"invoicePay" forKey:@"method"];
                    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
                    [details setObject:[_selectedCredit objectForKey:@"amount"] forKey:@"amount"];
                    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] forKey:@"customer_id"];
                    [details setObject:token.tokenId forKey:@"token"];
                    [details setObject:[_selectedCredit objectForKey:@"id"] forKey:@"id"];
                    
                    [api payForInvoceWithDetails:details cardParams:cardParams];
                }
                else if (_isBuyingCredits==YES)
                {
                    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
                    [details setObject:@"updateCredit" forKey:@"method"];
                    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
                    [details setObject:[_selectedCredit objectForKey:@"number_of_credits"] forKey:@"credit"];
                    [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] forKey:@"customer_id"];
                    [details setObject:token.tokenId forKey:@"token"];
                    if (_discountDetails)
                    {
                        [details setObject:[NSNumber numberWithFloat:([[_selectedCredit objectForKey:@"price"] floatValue]*(100-[[_discountDetails objectForKey:@"discount"] floatValue]))/100] forKey:@"amount"];
                    }
                    else
                    {
                        [details setObject:[_selectedCredit objectForKey:@"price"] forKey:@"amount"];
                    }
                    
                    [api buyCreditsWithDetails:details cardParams:cardParams];
                }
                else
                {
                    [api saveCardWithToken:token.tokenId];
                }
            }
        }];
    }
    else
    {
        [Helper showAlertViewWithTitle:@"Invalid" message:@"Invalid card details"];
        [Helper hideIndicatorFromView:self.view];
    }
}

+(NSBundle *)getResourcesBundle
{
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"Stripe" withExtension:@"bundle"]];
    return bundle;
}


+(UIImage *)loadImageFromResourceBundle:(NSString *)imageName
{
    NSBundle *bundle = [self getResourcesBundle];
    NSString *imageFileName = [NSString stringWithFormat:@"%@.png",imageName];
    UIImage *image = [UIImage imageNamed:imageFileName inBundle:bundle compatibleWithTraitCollection:nil];
    return image;
}

#pragma mark -- AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
            [details setObject:@"updateCredit" forKey:@"method"];
            [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
            [details setObject:[_selectedCredit objectForKey:@"number_of_credits"] forKey:@"credit"];
            [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] forKey:@"customer_id"];
            NSDictionary *dict = [cardList objectAtIndex:selectedIndex];
            [details setObject:[dict objectForKey:@"id"] forKey:@"card_id"];
            
            if (_discountDetails)
            {
                [details setObject:[NSNumber numberWithFloat:([[_selectedCredit objectForKey:@"price"] floatValue]*(100-[[_discountDetails objectForKey:@"discount"] floatValue]))/100] forKey:@"amount"];
            }
            else
            {
                [details setObject:[_selectedCredit objectForKey:@"price"] forKey:@"amount"];
            }
            
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
            [api buyCreditsWithDetails:details];
        }
    }
    else if (alertView.tag==2)
    {
        if (buttonIndex==1)
        {
            NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
            [details setObject:@"invoicePay" forKey:@"method"];
            [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"uid"];
            [details setObject:[_selectedCredit objectForKey:@"amount"] forKey:@"amount"];
            [details setObject:[_selectedCredit objectForKey:@"id"] forKey:@"id"];
            [details setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] forKey:@"customer_id"];
            NSDictionary *dict = [cardList objectAtIndex:selectedIndex];
            [details setObject:[dict objectForKey:@"id"] forKey:@"card_id"];
            
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
            [api payForInvoceWithDetails:details cardParams:nil];
        }
    }
}

#pragma mark -- PickerView Delegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        return monthList.count;
    }
    else
    {
        return [yearList count];
    }
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        [self showMonthForSelectedIndex:row];
    }
    else
    {
        [self showYearForSelectedIndex:row];
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==1)
    {
        return [monthList objectAtIndex:row];
    }
    else
    {
        return [yearList objectAtIndex:row];
    }
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (self.view.frame.size.width==375)
    {
        if (textField.tag==5)
        {
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-120, self.view.frame.size.width, self.view.frame.size.height)];
        }
    }
    else if (self.view.frame.size.height==568)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-200, self.view.frame.size.width, self.view.frame.size.height)];
    }
    [UIView commitAnimations];
    
    if (textField.tag==2)
    {
        [self showMonthPickerView];
    }
    else if (textField.tag==3)
    {
        [self showYearPickerView];
    }
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (activeField == textField)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
    else
    {
        if (self.view.frame.size.width==375)
        {
            if (textField.tag==5)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+120, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
        else if (self.view.frame.size.height==568)
        {
            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+200, self.view.frame.size.width, self.view.frame.size.height)];
        }
    }
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==5)
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
