//
//  AppointmentsViewController.h
//  LittleThings
//
//  Created by Saurav on 11/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "CreateAppointmentViewController.h"
#import "AppointmentDetailViewController.h"

@interface AppointmentsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, APIsDelegate>
{
    WebAPI *api;
    NSArray *appointmentList;
}

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIButton *appointmentBtn;
@property (strong, nonatomic) IBOutlet UILabel *appointmentLbl;
@property (strong, nonatomic) NSDate *selectedDate;

- (IBAction)addAppointment:(id)sender;
- (IBAction)back:(id)sender;
@end
