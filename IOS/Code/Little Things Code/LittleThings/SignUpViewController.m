//
//  SignUpViewController.m
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _registerBnt.layer.cornerRadius = 45/2;
    _registerBnt.clipsToBounds=YES;
    
    _photoLbl.layer.cornerRadius = 70/2;
    _photoLbl.clipsToBounds=YES;
    
    _userImage.layer.cornerRadius = 70/2;
    _userImage.clipsToBounds=YES;
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_loginLbl.text];
    //Already have an Account?Login Now
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:212/255.0 green:151/255.0 blue:151/255.0 alpha:1] range:NSMakeRange(28, 9)];
    [_loginLbl setAttributedText: text];
    
    _loginLbl.userInteractionEnabled=YES;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(login)];
    gesture.numberOfTapsRequired=1;
    [_loginLbl addGestureRecognizer:gesture];
    
    _userImage.userInteractionEnabled=YES;
    UITapGestureRecognizer *imagegesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectImage)];
    gesture.numberOfTapsRequired=1;
    [_userImage addGestureRecognizer:imagegesture];
    
    UIImageView *dropImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    dropImage.contentMode = UIViewContentModeCenter;
    dropImage.image = [UIImage imageNamed:@"down-arrow.png"];
    _gender.rightView = dropImage;
    _gender.rightViewMode = UITextFieldViewModeAlways;
    
    MMNumberKeyboard *keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _phone.inputView = keyboard;
    
    keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
    keyboard.allowsDecimalPoint = NO;
    _zip.inputView = keyboard;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    _termsLbl.userInteractionEnabled=YES;
    UITapGestureRecognizer *termsgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTerms)];
    termsgesture.numberOfTapsRequired=1;
    [_termsLbl addGestureRecognizer:termsgesture];
}

#pragma mark -- Button Action

- (IBAction)signUp:(id)sender
{
    if (_name.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Name cannot be empty."];
    }
    else if (_email.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Email cannot be empty."];
    }
    else if ([Helper validateEmailWithString:_email.text]==NO)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter valid email address."];
    }
    else if (_password.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Password cannot be empty."];
    }
    else if (_confirm.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please confirm your password."];
    }
    else if (![_confirm.text isEqualToString:_password.text])
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Password did not match."];
    }
    else if (_phone.text.length==0)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Phone number cannot be empty."];
    }
    else if (_phone.text.length!=10)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please enter 10 didgit phone number."];
    }
    else if (isTermsAccepted==NO)
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please accept all terms and conditions."];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [self.view endEditing:YES];
        
        NSMutableDictionary *details = [NSMutableDictionary dictionary];
        [details setObject:_name.text forKey:@"name"];
        [details setObject:_email.text forKey:@"email"];
        [details setObject:_password.text forKey:@"password"];
        [details setObject:_gender.text forKey:@"gender"];
        [details setObject:_phone.text forKey:@"phone_number"];
        [details setObject:_city.text forKey:@"city"];
        [details setObject:_state.text forKey:@"state"];
        [details setObject:_zip.text forKey:@"zipcode"];
        [details setObject:_address.text forKey:@"address"];
        [details setObject:@"1" forKey:@"deviceType"];
        [details setObject:[Helper getNotificationToken] forKey:@"deviceToken"];
        
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api signupUserWithDetails:details];
    }
}

- (IBAction)terms:(id)sender
{
    if (isTermsAccepted==NO)
    {
        isTermsAccepted=YES;
        [_termsBtn setImage:[UIImage imageNamed:@"filled_check.png"] forState:UIControlStateNormal];
    }
    else
    {
        isTermsAccepted=NO;
        [_termsBtn setImage:[UIImage imageNamed:@"empty_check.png"] forState:UIControlStateNormal];
    }
}

-(void)login
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)selectImage
{
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Capture Image", @"Photo Library", nil];
    sheet.tag=2;
    [sheet showInView:self.view];
}

-(void)viewTerms
{
//    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.thelittlethings.company/terms-and-conditions"]];
}

#pragma mark -- Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==1)
    {
        if (buttonIndex==0)
        {
            _gender.text = @"Male";
        }
        else if(buttonIndex == 1)
        {
            _gender.text = @"Female";
        }
    }
    else if (actionSheet.tag==2)
    {
        if (buttonIndex == 0)
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                controller.sourceType = UIImagePickerControllerSourceTypeCamera;
                controller.delegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"No Camera!" message:@"Camera not available!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil] show];
            }
        }
        else if (buttonIndex == 1)
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

#pragma mark -- ImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    _userImage.image=image;
    _userImage.contentMode=UIViewContentModeScaleAspectFill;
    _userImage.layer.cornerRadius=70/2;
    _userImage.clipsToBounds=YES;
    //NSData *imageData = UIImageJPEGRepresentation(image, 0.2);
    //imageString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackSignUpUserSuccess:(id)response
{
    //[Helper registerForSinch];
    NSString *receiverID=[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"email"]];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.client=nil;
    [appDelegate initSinchClientWithUserId:receiverID];
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"sw"];
    BuyCreditsViewController *buycontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"BuyCreditsViewController"];
    buycontroller.showBackBtn=YES;
    [self.navigationController setViewControllers:@[controller, buycontroller] animated:YES];
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==5)
    {
        [self.view endEditing:YES];
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"Gender" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Male", @"Female", nil];
        sheet.tag=1;
        [sheet showInView:self.view];
        return NO;
    }
    else
    {
        activeField=textField;
        if (self.view.frame.size.width==375)
        {
            if (textField.tag==1 && _scroll.contentOffset.y==-20)
            {
                _scroll.contentOffset = CGPointMake(0, 303);
            }
            NSLog(@"%f", _scroll.contentOffset.y);
            if (textField.tag == 6 || textField.tag == 7 || textField.tag == 9)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-250, self.view.frame.size.width, self.view.frame.size.height)];
                [UIView commitAnimations];
            }
            else if (textField.tag==8 || textField.tag == 10)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-220, self.view.frame.size.width, self.view.frame.size.height)];
                [UIView commitAnimations];
            }
        }
        else if (self.view.frame.size.height==568)
        {
            if (textField.tag==1 && _scroll.contentOffset.y==-20)
            {
                _scroll.contentOffset = CGPointMake(0, 450);
            }
            if (textField.tag == 6 || textField.tag == 7 || textField.tag == 8 || textField.tag == 9 || textField.tag == 10)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-180, self.view.frame.size.width, self.view.frame.size.height)];
                [UIView commitAnimations];
            }
        }
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField==activeField)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
    else
    {
        if (self.view.frame.size.width==375)
        {
            if (textField.tag == 6 || textField.tag == 7 || textField.tag == 9)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+250, self.view.frame.size.width, self.view.frame.size.height)];
            }
            else if (textField.tag==8 || textField.tag == 10)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+220, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
        else if (self.view.frame.size.height==568)
        {
            if (textField.tag == 6 || textField.tag == 7 || textField.tag == 8 || textField.tag == 9 || textField.tag == 10)
            {
                [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+180, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    }
    [UIView commitAnimations];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_email becomeFirstResponder];
    }
    else if (textField.tag==2)
    {
        [_password becomeFirstResponder];
    }
    else if (textField.tag==3)
    {
        [_confirm becomeFirstResponder];
    }
    else if (textField.tag==6)
    {
        [_city becomeFirstResponder];
    }
    else if (textField.tag==7)
    {
        [_state becomeFirstResponder];
    }
    else if (textField.tag==9)
    {
        [_zip becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
