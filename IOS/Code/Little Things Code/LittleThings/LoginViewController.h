//
//  LoginViewController.h
//  LittleThings
//
//  Created by Saurav on 10/03/17.
//  Copyright © 2017 TechEclat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "AppDelegate.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate, APIsDelegate>
{
    UITextField *activeField;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UILabel *signUpLbl;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
- (IBAction)login:(id)sender;
- (IBAction)Forgot:(id)sender;
@end
