//
//  DrawerCell.h
//  canoe
//
//  Created by HashBrown Systems on 26/11/14.
//  Copyright (c) 2014 Hashbrown Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawerCell : UITableViewCell

@property (strong, nonatomic) UIImageView *icon;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *credits;
@end
